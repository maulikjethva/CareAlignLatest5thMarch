/**
 * Copyright (c) 2015 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.rtwo.med.device.connect.util.loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

/**
 * @author MAHESH
 *
 */
public enum ConfigurationLoader {

	INSTANCE;
	
	private String envVar;
	
	private ConfigurationLoader() {
	}
	
	public void registerSysEnvVariable(String var){
		
		this.envVar = var;
	}
	
	public String getFilePath(String dir){
		
		if(StringUtils.isBlank(this.envVar)){
			
			throw new RuntimeException("System Environment Variable is mandatory.");
		}
		
		String home = System.getenv(this.envVar);
		return new StringBuilder()
		.append(home).append(File.separatorChar).append(dir)
		.append(File.separatorChar).toString();
	}
	
	public String getTypedFile(String dir, String fileName, FileType fileType){
		
		if(StringUtils.isBlank(this.envVar)){
			
			throw new RuntimeException("System Environment Variable is mandatory.");
		}
		return new StringBuilder().append(this.getFilePath(dir)).append(fileName).append(".").append(fileType.getName()).toString();
	}
	
	public Properties getProperties(String dir, String fileName){
		
		if(StringUtils.isBlank(this.envVar)){
			
			throw new RuntimeException("System Environment Variable is mandatory.");
		}
		
		try{
			
			InputStream stream = this.getFileAsInputStream(dir, fileName, FileType.PROPERTY);
			Properties prop = new Properties();
			prop.load(stream);
			return prop;
			
		}
		catch(IOException e){
			
			throw new RuntimeException("Property File Error.", e);
		}
	}
	
	public InputStream getFileAsInputStream(String dir, String fileName, FileType fileType){
		
		try{
			
			return new FileInputStream(this.getTypedFile(dir, fileName, fileType));
		}
		catch(FileNotFoundException e){
			
			throw new RuntimeException("File Not Found.", e);
		}
	}
	
	public Map<String, String> getPropertiesMap(String dir, String fileName){
		
		Map<String, String> map = new HashMap<String, String>();
		Properties prop = this.getProperties(dir, fileName);
		for(Entry<Object, Object> entry : prop.entrySet()){
			
			map.put((String)entry.getKey(), (String)entry.getValue());
		}
		return map;
	}
}
