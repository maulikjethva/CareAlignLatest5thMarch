/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.rtwo.med.device.connect.util.math;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.List;

/**
 * @author MAHESH
 *
 */
public final class MathUtil {

	private MathUtil() {
		
	}
	
	/**
	 * 
	 * @param buffer
	 * @return
	 */
	public static Number[] hexToNumbers(ByteBuffer buffer){
		byte[] byts = buffer.array();
		int size = byts.length;
		Number[] numbers = new Number[size];
		for(int i=0; i< size; i++){
			int ival = 0xFF & byts[i];
			numbers[i] = ival;
		}
		return numbers;
	}
	
	public static float[] hexTofloats(ByteBuffer buffer){
		byte[] byts = buffer.array();
		int size = byts.length;
		float[] numbers = new float[size];
		for(int i=0; i< size; i++){
			float ival = 0xFF & byts[i];
			numbers[i] = ival;
		}
		return numbers;
	}
	
	/**
	 * 
	 * @param n
	 * @return
	 */
	public static List<Integer> getONBitPositions(Number n){
		
		List<Integer> list = new ArrayList<Integer>();
		String binaryStr = Integer.toBinaryString(n.intValue());
		binaryStr = String.format("%8s", binaryStr).replace(' ', '0');
		for(int i=0; i< binaryStr.length(); i++){
			
			if(Character.getNumericValue(binaryStr.charAt(i)) == 1){
				
				list.add(i);
			}
		}
		return list;
	}
	
	public static List<Integer> getReversedONBitPositions(Number n){
		
		List<Integer> list = new ArrayList<Integer>();
		String binaryStr = Integer.toBinaryString(n.intValue());
		binaryStr = String.format("%8s", binaryStr).replace(' ', '0');
		
		StringBuilder strBlr = new StringBuilder(binaryStr);
		binaryStr = strBlr.reverse().toString();
		for(int i=0; i< binaryStr.length(); i++){
			
			if(Character.getNumericValue(binaryStr.charAt(i)) == 1){
				
				list.add(i);
			}
		}
		return list;
	}
	
	/**
	 * 
	 * @param n
	 * @return
	 */
	public static Integer getONBitPosition(Number n){
		
		String binaryStr = Integer.toBinaryString(n.intValue());
		binaryStr = String.format("%8s", binaryStr).replace(' ', '0');
		for(int i=0; i< binaryStr.length(); i++){
			
			if(Character.getNumericValue(binaryStr.charAt(i)) == 1){
				
				return i;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param input
	 * @param times
	 * @param charsetName
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String encodeMultiTimes(String input, int times, String charsetName) throws UnsupportedEncodingException{
		
		Encoder encoder = Base64.getEncoder();
		String encodedStr = encoder.encodeToString(input.getBytes(charsetName));
		for(int i=0; i<=times; i++){
			encodedStr = encoder.encodeToString(encodedStr.getBytes(charsetName));
		}
		return encodedStr;
	}
	
	/**
	 * 
	 * @param input
	 * @param times
	 * @param charsetName
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String decodeMultiTimes(String input, int times, String charsetName) throws UnsupportedEncodingException{
		
		Decoder decoder = Base64.getDecoder();
		byte[] decodeBytes = decoder.decode(input);
		for(int i=0; i<=times; i++){
			decodeBytes = decoder.decode(decodeBytes);
		}
		return new String(decodeBytes, charsetName);
	}
}
