/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.rtwo.med.device.connect.util.crypto;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import org.apache.commons.lang3.StringUtils;

/**
 * @author MAHESH
 *
 */
public final class IdUtil {

	private static final Encoder encoder = Base64.getEncoder();
	
	private static final Decoder decoder = Base64.getDecoder();

	private IdUtil() {

	}
	
	/**
	 * 
	 * @param attrs
	 * @return
	 */
	public static synchronized String createComplexId(String... attrs){//TODO: Need to make it real complex which can't be hacked.
		
		StringBuilder builder = new StringBuilder();
		for (String attr : attrs) {

			String temp = StringUtils.deleteWhitespace(attr);
			temp = StringUtils.replace(temp, ".", "dot");
			builder.append(temp).append("_");
		}
		return StringUtils.removeEnd(builder.toString(), "_");
	}
	
	/**
	 * 
	 * @param oldComplexId
	 * @param newComplexId
	 * @return
	 */
	public static synchronized String updateComplexId(String oldComplexId, String newComplexId, int updatePos){//TODO: Need to make it real complex which can't be hacked.
		
		if(updatePos == -1){
			
			return oldComplexId;
		}
		String[] oldParts = StringUtils.split(oldComplexId, "_");
		String[] newParts = StringUtils.split(newComplexId, "_");
		if(oldParts.length != newParts.length){
			
			System.out.println("Id's are in different length!!!");
			return null;
		}
		StringBuilder updatedIdBuilder = new StringBuilder();
		for(int i=0; i< oldParts.length; i++){
			
			if((!oldParts[i].equals(newParts[i])) && i == updatePos){
				updatedIdBuilder.append(newParts[i]);
			}else{
				updatedIdBuilder.append(oldParts[i]);
			}
			updatedIdBuilder.append("_");
		}
		return StringUtils.removeEnd(updatedIdBuilder.toString(), "_");
	}
	
	public static synchronized String encrypt(String input) {

		try {

			input = encoder.encodeToString(input.getBytes("UTF-8"));

		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		}
		return StringUtils.reverse(input);
	}
	
	public static synchronized String decrypt(String input) {

		try {

			input = StringUtils.reverse(input);
			byte[] decodedBytes = decoder.decode(input.getBytes("UTF-8"));
			input = new String(decodedBytes, "UTF-8");

		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		}
		return input;
	}
	
	/*public static synchronized String[] complexIdAttrs(String cId){
		
		String[] parts = StringUtils.split(cId, "_");
		String[] attrs = new String[parts.length];
		for(int i=0; i < parts.length; i++){
			
			attrs[i] = StringUtils.replace(parts[i], "dot", ".");
		}
		return attrs;
	}

	// TODO: Make it perfect...
	public static String uniqueIdBuilder(String... attrs) {

		StringBuilder builder = new StringBuilder();
		for (String attr : attrs) {

			String temp = StringUtils.deleteWhitespace(attr);
			temp = StringUtils.replace(temp, ".", "dot");
			builder.append(temp);
		}
		String id = encrypt(builder.toString());
		return StringUtils.replace(id, "=", "");
	}
	
	public static IdParam getuniqueIdParam(String uniqueId){
		
		String decryptStr = decrypt(uniqueId);
		return new IdParam("", "", "", "");
	}*/

	/*public static class IdParam{
		
		private String centerId;
		private String bedId;
		private String bedIpId;
		private String deviceId;
		
		public IdParam(String centerId, String bedId, String bedIpId, String deviceId) {
			this.centerId = centerId;
			this.bedId = bedId;
			this.bedIpId = bedIpId;
			this.deviceId = deviceId;
		}

		public String getCenterId() {
			return centerId;
		}

		public String getBedId() {
			return bedId;
		}

		public String getBedIpId() {
			return bedIpId;
		}

		public String getDeviceId() {
			return deviceId;
		}
	}*/
}
