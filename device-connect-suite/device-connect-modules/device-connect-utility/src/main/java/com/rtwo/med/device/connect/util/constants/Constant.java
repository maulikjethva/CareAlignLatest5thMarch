/**
 * Copyright (c) 2015 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.rtwo.med.device.connect.util.constants;

/**
 * @author MAHESH
 *
 */
public final class Constant {

	private Constant() {}
	
	public static final String COMMA = ",";
	
	public static final String DEGREE_CELSIUS = "\u00b0"+"C";
	
	public static final String PERCENTAGE = "%";
	
	public static final String GRAMS = "grams";
	
	public static final String KILO_GRAM = "kg";
	
	public static final String BPM = "bpm";
	
	public static final String WEEKS = "wk";
	
	public static final String MGPERDL = "mg/dl";
	
	public static final String IP = "IP";
	
	public static final String LABEL = "LABEL";
}
