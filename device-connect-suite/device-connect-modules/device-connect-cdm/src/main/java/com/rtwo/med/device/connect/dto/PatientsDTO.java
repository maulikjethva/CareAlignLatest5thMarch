/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

package com.rtwo.med.device.connect.dto;

import java.util.Date;

/**
 *
 * 
 */
public class PatientsDTO {
	private String patientGender;
	private String patientSelectedMedicalCondition;
	private String patientChiefComplaint;
	private Date startTime;
	private Date endTime;
	private String hospitalName;
	private String dentist_id;
	private String dentist;
	private String treatment;
	private String patientPlace;
	private String patientName;
	private String patientAge;
	private String emailId;
	private String contact;
	private Date created;
	
	public String getPatientGender() {
		return patientGender;
	}
	public void setPatientGender(String patientGender) {
		this.patientGender = patientGender;
	}
	public String getPatientSelectedMedicalCondition() {
		return patientSelectedMedicalCondition;
	}
	public void setPatientSelectedMedicalCondition(String patientSelectedMedicalCondition) {
		this.patientSelectedMedicalCondition = patientSelectedMedicalCondition;
	}
	public String getPatientChiefComplaint() {
		return patientChiefComplaint;
	}
	public void setPatientChiefComplaint(String patientChiefComplaint) {
		this.patientChiefComplaint = patientChiefComplaint;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getDentist_id() {
		return dentist_id;
	}
	public void setDentist_id(String dentist_id) {
		this.dentist_id = dentist_id;
	}
	public String getDentist() {
		return dentist;
	}
	public void setDentist(String dentist) {
		this.dentist = dentist;
	}
	public String getTreatment() {
		return treatment;
	}
	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}
	public String getPatientPlace() {
		return patientPlace;
	}
	public void setPatientPlace(String patientPlace) {
		this.patientPlace = patientPlace;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getPatientAge() {
		return patientAge;
	}
	public void setPatientAge(String patientAge) {
		this.patientAge = patientAge;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
}
