package com.rtwo.med.device.connect.dto;

public class ValuesDTO {
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
