/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */


package com.rtwo.med.device.connect.dto;

import java.security.Principal;

/**
 * 
 * This is DTO class
 *
 */
public class AuthenticateUserDTO implements Principal {
	
	private String userName;
	private boolean authenticate;
	private boolean administrator;
	private String sessionId;
	private String[] roles;
	
	/**
	 * 
	 * @param userName the userName to set
	 */
	public AuthenticateUserDTO(String userName){
		this.userName=userName;
	}
	/**
	 * @return the userName
	 */
	public String getName() {
		return userName;
	}
	/**
	 * 
	 * @return the authenticate
	 */
	public boolean isAuthenticate() {
		return authenticate;
	}
	/**
	 * 
	 * @param authenticate the authenticate to set
	 */
	public void setAuthenticate(boolean authenticate) {
		this.authenticate = authenticate;
	}
	/**
	 * 
	 * @return the administrator
	 */
	public boolean isAdministrator() {
		return administrator;
	}
	/**
	 * 
	 * @param administrator the administrator to set
	 */
	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}
	/**
	 * 
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * 
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String[] getRoles() {
		return roles;
	}
	public void setRoles(String[] roles) {
		this.roles = roles;
	}	
}
