/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

package com.rtwo.med.device.connect.dto;

import java.util.List;

public class UserEntityDTO {

	private String logId;
	private String password;
	private String fName;
	private String lName;
	private String email;
	private String phone;
	private List<String> roles;
	private boolean patientModificationAllowed;
	private boolean bedModificationAllowed;

	/**
	 * 
	 * @return the logId
	 */
	public String getLogId() {
		return logId;
	}

	/**
	 * 
	 * @param logId
	 *            the logId to set
	 */
	public void setLogId(String logId) {
		this.logId = logId;
	}

	/**
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 
	 * @return the first name
	 */
	public String getfName() {
		return fName;
	}

	/**
	 * 
	 * @param fName
	 *            the first name to set
	 */
	public void setfName(String fName) {
		this.fName = fName;
	}

	/**
	 * 
	 * @return the last name
	 */
	public String getlName() {
		return lName;
	}

	/**
	 * 
	 * @param lName
	 *            the last name to set
	 */
	public void setlName(String lName) {
		this.lName = lName;
	}

	/**
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * 
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 
	 * @return the roles
	 */
	public List<String> getRoles() {
		return roles;
	}

	/**
	 * 
	 * @param roles
	 *            the roles to set
	 */
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	/**
	 * 
	 * @return the patient modification permission status
	 */
	public boolean getPatientModificationAllowed() {
		return patientModificationAllowed;
	}

	/**
	 * 
	 * @param patientModificationAllowed
	 *            the patient modification permission status to set
	 */
	public void setPatientModificationAllowed(boolean patientModificationAllowed) {
		this.patientModificationAllowed = patientModificationAllowed;
	}

	/**
	 * 
	 * @return the bed modification permission status
	 */
	public boolean getBedModificationAllowed() {
		return bedModificationAllowed;
	}

	/**
	 * 
	 * @param bedModificationAllowed
	 *            the bed modification permission status to set
	 */
	public void setBedModificationAllowed(boolean bedModificationAllowed) {
		this.bedModificationAllowed = bedModificationAllowed;
	}
}
