package com.rtwo.med.device.connect.dto;

public class ValueRefDTO {
	
	private String value;
	private String ref;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}

}
