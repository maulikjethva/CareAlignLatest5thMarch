package com.rtwo.med.device.connect.mongo.dal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.rtwo.med.device.connect.mongo.dal.entities.Surfacevalue;

public interface SurfaceValueRepository extends MongoRepository<Surfacevalue, String> {

}
