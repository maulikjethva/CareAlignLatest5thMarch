package com.rtwo.med.device.connect.mongo.dal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.rtwo.med.device.connect.mongo.dal.entities.Treatmentvalue;

public interface TreatmentValueRepository extends MongoRepository<Treatmentvalue, String> {

}
