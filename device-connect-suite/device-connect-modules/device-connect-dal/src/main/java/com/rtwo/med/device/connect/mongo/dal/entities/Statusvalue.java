package com.rtwo.med.device.connect.mongo.dal.entities;

public class Statusvalue {
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
