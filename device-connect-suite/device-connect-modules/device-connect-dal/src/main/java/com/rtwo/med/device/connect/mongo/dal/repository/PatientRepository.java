package com.rtwo.med.device.connect.mongo.dal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.rtwo.med.device.connect.mongo.dal.entities.Patients;

public interface PatientRepository extends MongoRepository<Patients, String> {

	Patients findBypatientName(String patientName);

}
