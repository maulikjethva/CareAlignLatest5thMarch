package com.rtwo.med.device.connect.mongo.dal.entities;

public class Consultationexamination {
	
	private String category;
	private String performedBy;
	private String performedDate;
	private String plannedDate;
	private String remark;
	private String specialty;
	private String status;
	private String surface;
	private String teethNo;
	private String treatment;
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPerformedBy() {
		return performedBy;
	}
	public void setPerformedBy(String performedBy) {
		this.performedBy = performedBy;
	}
	public String getPerformedDate() {
		return performedDate;
	}
	public void setPerformedDate(String performedDate) {
		this.performedDate = performedDate;
	}
	public String getPlannedDate() {
		return plannedDate;
	}
	public void setPlannedDate(String plannedDate) {
		this.plannedDate = plannedDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSpecialty() {
		return specialty;
	}
	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSurface() {
		return surface;
	}
	public void setSurface(String surface) {
		this.surface = surface;
	}
	public String getTeethNo() {
		return teethNo;
	}
	public void setTeethNo(String teethNo) {
		this.teethNo = teethNo;
	}
	public String getTreatment() {
		return treatment;
	}
	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

}
