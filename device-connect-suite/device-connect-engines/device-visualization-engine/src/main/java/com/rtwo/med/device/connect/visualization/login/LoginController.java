/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

package com.rtwo.med.device.connect.visualization.login;

import java.security.Principal;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;

import com.rtwo.med.device.connect.dto.AuthenticateUserDTO;

@RestController
public class LoginController {
	/**
	 * 
	 * Receive Authenticated user information and return Authenticated user info
	 * based on the user role
	 */
	@RequestMapping("/login")
	public Principal user(Principal user) {

		String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (user != null && user.getName() != null && sessionId != null) {
			AuthenticateUserDTO authenticateUserdto = new AuthenticateUserDTO(user.getName());
			authenticateUserdto.setAuthenticate(true);
			authenticateUserdto.setSessionId(sessionId);

			String[] roles = new String[authentication.getAuthorities().size()];
			int i = 0;
			
			for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {

				roles[i++] = grantedAuthority.getAuthority();

				boolean isAdmin = grantedAuthority.getAuthority().equalsIgnoreCase("Administrator");
				if (isAdmin) {
					authenticateUserdto.setAdministrator(true);
					authenticateUserdto.setRoles(roles);
					return authenticateUserdto;
				}
			}
			
			authenticateUserdto.setRoles(roles);
			authenticateUserdto.setAdministrator(false);
			
			return authenticateUserdto;
		}
		return null;
	}
}
