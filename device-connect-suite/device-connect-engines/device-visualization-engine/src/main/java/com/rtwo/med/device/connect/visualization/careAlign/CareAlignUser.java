package com.rtwo.med.device.connect.visualization.careAlign;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CareAlignUser {
	
	private int iUserID;
	private String strUserName;
	private String strUserPassword;
	private String strUserFirstName;
	private String strUserLastName;
	private String strEmailId;
	private String strPhone;
	private String strAddress;
	private String strGender;
	private String strCity;
	private String strUserID;
	private String strUserRegDate;
	private String strSalRepUserName;
	
	public String getStrUserRegDate() {
		return strUserRegDate;
	}

	public void setStrUserRegDate(String strUserRegDate) {
		this.strUserRegDate = strUserRegDate;
	}

	public String getStrUserID() {
		return strUserID;
	}

	public void setStrUserID(String strUserID) {
		this.strUserID = strUserID;
	}

	public String getStrCity() {
		return strCity;
	}

	public void setStrCity(String strCity) {
		this.strCity = strCity;
	}

	private int iReferedBy;
	private int iRole;
	private int iAccept;
	
	private CareAlignDB objDB;
	
	public void setDB(CareAlignDB obj)
	{
		objDB = obj;
	}

	private int getiUserID() {
		return iUserID;
	}
	private void setiUserID(int iUserID) {
		this.iUserID = iUserID;
	}
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	private String getStrUserPassword() {
		return strUserPassword;
	}
	private void setStrUserPassword(String strUserPassword) {
		this.strUserPassword = strUserPassword;
	}
	public String getStrUserFirstName() {
		return strUserFirstName;
	}
	public void setStrUserFirstName(String strUserFirstName) {
		this.strUserFirstName = strUserFirstName;
	}
	public String getStrUserLastName() {
		return strUserLastName;
	}
	public void setStrUserLastName(String strUserLastName) {
		this.strUserLastName = strUserLastName;
	}
	public String getStrEmailId() {
		return strEmailId;
	}
	public void setStrEmailId(String strEmailId) {
		this.strEmailId = strEmailId;
	}
	public String getStrPhone() {
		return strPhone;
	}
	public void setStrPhone(String strPhone) {
		this.strPhone = strPhone;
	}
	public String getStrAddress() {
		return strAddress;
	}
	public void setStrAddress(String strAddress) {
		this.strAddress = strAddress;
	}
	public String getStrGender() {
		return strGender;
	}
	public void setStrGender(String strGender) {
		this.strGender = strGender;
	}
	public int getStrReferedBy() {
		return iReferedBy;
	}
	public void setStrReferedBy(int iReferedBy) {
		this.iReferedBy = iReferedBy;
	}
	private int getiRole() {
		return iRole;
	}
	private void setiRole(int iRole) {
		this.iRole = iRole;
	}
	private int getiAccept() {
		return iAccept;
	}
	private void setiAccept(int iAccept) {
		this.iAccept = iAccept;
	}

	public CareAlignUser authenticateUser( String userName, String passWord )
	{
		
		if(objDB.authenticate(userName, passWord) == true)
		{
			Map<String,String> map=new HashMap<String,String>(); 
			map = objDB.getUserDetails(userName, passWord);
			 for(Map.Entry m:map.entrySet()){  
				   System.out.println(m.getKey()+" "+m.getValue());
				   String key = (String) m.getKey();
				   switch(key)
				   {
				   		case "UserID":
				   			String strId = (String) m.getValue();
				   			this.iUserID = Integer.parseInt(strId);
				   			break;
				   		case "First_Name":
				   			this.strUserFirstName = (String) m.getValue();
				   			break;
				   		case "Last_Name":
				   			this.strUserLastName = (String) m.getValue();
				   			break;
				   		case "Role":
				   			String str = (String) m.getValue();
				   			this.iRole = Integer.parseInt(str);
				   			break;
				   		case "Mobile_Number":
				   			this.strPhone = (String) m.getValue();
				   			break;
				   		case "Email_ID":
				   			this.strEmailId = (String) m.getValue();
				   			break;
				   		case "Address":
				   			this.strAddress = (String) m.getValue();
				   			break;
				   		case "City":
				   			this.strCity = (String) m.getValue();
				   			break;
				   		case "Reg_By":
				   			String strRef = (String) m.getValue();
				   			this.iReferedBy = Integer.parseInt(strRef);
				   			break;
				   		case "Gender":
				   			this.strGender = (String) m.getValue();
				   			break;
				   		case "Accept":
				   			String strAcc = (String) m.getValue();
				   			this.iAccept = Integer.parseInt(strAcc);
				   			break;
				   		case "User_RegDate":
				   			this.strUserRegDate = (String) m.getValue();
				   			break;
				   }
				   this.strUserName = userName;
				  }  
			return this;
		}
		else
			return null;
	}
	
	public CareAlignUser getUserDetails(String userName, String passWord)
	{
		objDB.getUserDetails(userName, passWord);
		return this;
	}
	
	//called from dashboard
	public Map getUsersObjList(String userName, String passWord)
	{
		Map<String,Map> mapColl=new HashMap<String,Map>();
		Map<String,CareAlignUser> mapCollObj=new HashMap<String,CareAlignUser>();
		int iCounter = 0;
		mapColl = objDB.getUserDetails(userName, passWord);
		for(Map.Entry m:mapColl.entrySet()){ {
			iCounter++;
			CareAlignUser usrObj = new CareAlignUser();
			System.out.println(m.getKey()+" "+m.getValue());
			Map<String,String> map=new HashMap<String,String>();
			map = (Map<String, String>) m.getValue();
			for(Map.Entry ma:map.entrySet()){ 
				System.out.println(ma.getKey()+" "+ma.getValue());
				String key = (String) m.getKey();
				switch(key)
				{
			   		case "UserID":
			   			String strId = (String) m.getValue();
			   			usrObj.iUserID = Integer.parseInt(strId);
			   			break;
			   		case "First_Name":
			   			usrObj.strUserFirstName = (String) m.getValue();
			   			break;
			   		case "Last_Name":
			   			usrObj.strUserLastName = (String) m.getValue();
			   			break;
			   		case "Role":
			   			String str = (String) m.getValue();
			   			usrObj.iRole = Integer.parseInt(str);
			   			break;
			   		case "Mobile_Number":
			   			usrObj.strPhone = (String) m.getValue();
			   			break;
			   		case "Email_ID":
			   			usrObj.strEmailId = (String) m.getValue();
			   			break;
			   		case "Address":
			   			usrObj.strAddress = (String) m.getValue();
			   			break;
			   		case "City":
			   			usrObj.strCity = (String) m.getValue();
			   			break;
			   		case "Reg_By":
			   			String strRef = (String) m.getValue();
			   			usrObj.iReferedBy = Integer.parseInt(strRef);
			   			break;
			   		case "Gender":
			   			usrObj.strGender = (String) m.getValue();
			   			break;
			   		case "Accept":
			   			String strAcc = (String) m.getValue();
			   			usrObj.iAccept = Integer.parseInt(strAcc);
			   			break;
			   		case "User_RegDate":
			   			usrObj.strUserRegDate = (String) m.getValue();
			   			break;
			   }
			   usrObj.strUserName = userName;
			   mapCollObj.put(Integer.toString(iCounter), usrObj);
			  }
			}
		}
		return mapColl;
	}
	
	public boolean registerNewUser(CareAlignUser obj)
	{
		if(obj.iReferedBy == 1)
			return objDB.registerUser(obj.strUserName,obj.strUserFirstName,obj.strUserLastName,obj.strEmailId,obj.strPhone,obj.strGender,obj.strAddress,obj.iReferedBy,obj.strCity);
		else
			return objDB.registerUserBySalesRep(obj.strUserName,obj.strUserFirstName,obj.strUserLastName,obj.strEmailId,obj.strPhone,obj.strGender,obj.strAddress,obj.iReferedBy,obj.strCity,obj.strSalRepUserName);
		
	}
	
	public void uploadImage(String path)
	{
		FileInputStream image = null;
		File file = null;
		String name = "";
		try{
			file=new File(path);
			image =new FileInputStream(file);
		}catch(Exception e){
			System.out.println(e);
		}
		
		objDB.uploadImage(image, file,name);
	}
	
	public void downloadImage(String path)
	{
		objDB.downloadImage(path);
	}
/*	public void getUserList(String userName)
	{
		objDB.getUserList(userName);
	}*/

	public String getStrSalRepUserName() {
		return strSalRepUserName;
	}

	public void setStrSalRepUserName(String strSalRepUserName) {
		this.strSalRepUserName = strSalRepUserName;
	}
}
