/**
 * Copyright (c) 2015 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

package com.rtwo.med.device.connect.visualization.service;

import org.atmosphere.cpr.MetaBroadcaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.google.gson.GsonBuilder;

/**
 * 
 */
@Controller
public class DeviceVisualizationHttpService {

	/**
	 * Creates a new instance of {@link DeviceVisualizationHttpService}.
	 */
//	@Autowired
//	public DeviceVisualizationHttpService(MetaBroadcaster metaBroadcaster) {
//
//		GsonBuilder jsonBuilder = new GsonBuilder();
//		jsonBuilder.create();
//		if (metaBroadcaster == null) {
//			throw new NullPointerException("metaBroadcaster must not be null");
//		}
//
//		DeviceVisualizationRealTimeService.metaBroadcaster = metaBroadcaster;
//	}

	/**
	 * Home action.
	 * 
	 * @return the index page.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "index";
	}

}
