/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

package com.rtwo.med.device.connect.visualization.config;

import java.util.Collections;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.catalina.Context;
import org.apache.tomcat.websocket.server.WsSci;
import org.atmosphere.cpr.AtmosphereFramework;
import org.atmosphere.cpr.AtmosphereServlet;
import org.atmosphere.cpr.MetaBroadcaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.rtwo.med.device.connect.visualization.service.DeviceVisualizationRealTimeService;

/**
 * 
 */
@Configuration
//@EnableWebMvc
public class WebConfigurer /*extends WebMvcConfigurerAdapter*/ implements ServletContextInitializer {


  @Autowired
  private MetaBroadcaster metaBroadcaster;
	
  @Bean
  public TomcatEmbeddedServletContainerFactory tomcatContainerFactory() {
	
	DeviceVisualizationRealTimeService.metaBroadcaster = metaBroadcaster;
    TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
    factory.setTomcatContextCustomizers(Collections.singletonList(tomcatContextCustomizer()));
    return factory;
  }

  @Bean
  public TomcatContextCustomizer tomcatContextCustomizer() {
    return new TomcatContextCustomizer() {
      @Override
      public void customize(Context context) {
        context.addServletContainerInitializer(new WsSci(), null);
      }
    };
  }

  @Bean
  public AtmosphereServlet atmosphereServlet() {
    return new AtmosphereServlet();
  }

  @Bean
  public AtmosphereFramework atmosphereFramework() {
    return atmosphereServlet().framework();
  }

  @Bean
  public MetaBroadcaster metaBroadcaster() {
    AtmosphereFramework framework = atmosphereFramework();
    return framework.metaBroadcaster();
  }

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {
    configureAthmosphere(atmosphereServlet(), servletContext);
  }

  private void configureAthmosphere(AtmosphereServlet servlet, ServletContext servletContext) {
    ServletRegistration.Dynamic atmosphereServlet = servletContext.addServlet("atmosphereServlet", servlet);
//    atmosphereServlet.setInitParameter(ApplicationConfig.ANNOTATION_PACKAGE, DeviceVisualizationRealTimeService.class.getPackage().getName());
//    atmosphereServlet.setInitParameter(ApplicationConfig.BROADCASTER_CACHE, UUIDBroadcasterCache.class.getName());
//    atmosphereServlet.setInitParameter(ApplicationConfig.BROADCASTER_SHARABLE_THREAD_POOLS, "true");
//    atmosphereServlet.setInitParameter(ApplicationConfig.BROADCASTER_MESSAGE_PROCESSING_THREADPOOL_MAXSIZE, "10");
//    atmosphereServlet.setInitParameter(ApplicationConfig.BROADCASTER_ASYNC_WRITE_THREADPOOL_MAXSIZE, "10");
//    atmosphereServlet.setInitParameter(ApplicationConfig.HEARTBEAT_INTERVAL_IN_SECONDS, "25");//TODO: Need to Optimize
    servletContext.addListener(new org.atmosphere.cpr.SessionSupport());
    atmosphereServlet.addMapping("/device-connect/*");
    atmosphereServlet.setLoadOnStartup(0);
    atmosphereServlet.setAsyncSupported(true);
  }
  
  @Bean
  public Mongo mongo() throws Exception 
  {
      return new MongoClient("localhost" , 27017 );
  }
  
  @Bean
  public MongoTemplate mongoTemplate() throws Exception 
  {
      return new MongoTemplate(mongo(), "mean-dev");
  }

}
