/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.rtwo.med.device.connect.visualization.user;

/**
 * 
 * This is custom exception class
 *
 */
public class UserManagementException extends RuntimeException {
	
	 
	private static final long serialVersionUID = 1L;
	public String message = null;
	 
	public UserManagementException(){
		super();
	}
	/**
	 * 
	 * @param message
	 */
	public UserManagementException(String message){
		super(message);
		this.message = message;
	}
	
	public UserManagementException(String message, Exception e){
		super(message, e);
		this.message = message;
	}
	
	/**
	 * 
	 * @param cause
	 */
	public UserManagementException(Throwable cause){
		super(cause);
	}

}
