package com.rtwo.med.device.connect.visualization.careAlign;

import java.util.HashMap;
import java.util.Map;

public class CareAlignPatientInfo {

	private int iPatientID;
	private String strPatientFirstName;
	private String strPatientLastName;
	private String strPatientAddress;
	private String strPatientEmail;
	private String strPatientPhone;
	private String strPatientCity;
	private String strPatientGender;
	private CareAlignDB objDB;
	
	public CareAlignDB getObjDB() {
		return objDB;
	}
	public void setObjDB(CareAlignDB objDB) {
		this.objDB = objDB;
	}
	public int getiPatientID() {
		return iPatientID;
	}
	public void setiPatientID(int iPatientID) {
		this.iPatientID = iPatientID;
	}
	public String getStrPatientFirstName() {
		return strPatientFirstName;
	}
	public void setStrPatientFirstName(String strPatientFirstName) {
		this.strPatientFirstName = strPatientFirstName;
	}
	public String getStrPatientLastName() {
		return strPatientLastName;
	}
	public void setStrPatientLastName(String strPatientLastName) {
		this.strPatientLastName = strPatientLastName;
	}
	public String getStrPatientAddress() {
		return strPatientAddress;
	}
	public void setStrPatientAddress(String strPatientAddress) {
		this.strPatientAddress = strPatientAddress;
	}
	public String getStrPatientEmail() {
		return strPatientEmail;
	}
	public void setStrPatientEmail(String strPatientEmail) {
		this.strPatientEmail = strPatientEmail;
	}
	public String getStrPatientPhone() {
		return strPatientPhone;
	}
	public void setStrPatientPhone(String strPatientPhone) {
		this.strPatientPhone = strPatientPhone;
	}
	public String getStrPatientCity() {
		return strPatientCity;
	}
	public void setStrPatientCity(String strPatientCity) {
		this.strPatientCity = strPatientCity;
	}
	public String getStrPatientGender() {
		return strPatientGender;
	}
	public void setStrPatientGender(String strPatientGender) {
		this.strPatientGender = strPatientGender;
	}
	
	public boolean registerNewPatientDemo(String userName, String passWord, String patFName, String patLName, String patAddress, String patEmail, String patPhone, String patCity, String patGender)
	{
		return objDB.registerNewPatientDemo(userName,passWord, patFName, patLName, patAddress, patEmail, patPhone, patCity, patGender);
	}
	
	public CareAlignPatientInfo getPatientDemoDetails(String patFName,String patPhone)
	{
		Map<String,String> map=new HashMap<String,String>();
		map = objDB.getPatientDemoDetails(patFName, patPhone);
		
		for(Map.Entry m:map.entrySet()){  
			   System.out.println(m.getKey()+" "+m.getValue());
			   String key = (String) m.getKey();
			   switch(key)
			   {
			   		case "FirstName":
			   			this.strPatientFirstName = (String) m.getValue();
			   			break;
			   		case "LastName":
			   			this.strPatientLastName = (String) m.getValue();
			   			break;
			   		case "Address":
			   			this.strPatientAddress = (String) m.getValue();
			   			break;
			   		case "Email":
			   			this.strPatientEmail = (String) m.getValue();
			   			break;
			   		case "Phone":
			   	
			   			this.strPatientPhone = (String) m.getValue();
			   			break;
			   		case "Gender":
			   			this.strPatientGender = (String) m.getValue();
			   			break;
			   		case "City":
			   			this.strPatientCity = (String) m.getValue();
			   			break;
			   }
		}
		return this;
	}

}
