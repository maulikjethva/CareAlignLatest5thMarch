/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

package com.rtwo.med.device.connect.visualization.service;

import java.nio.charset.StandardCharsets;

import javax.websocket.OnMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.Get;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.PathParam;
import org.atmosphere.config.service.Ready;
import org.atmosphere.config.service.Singleton;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceFactory;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.MetaBroadcaster;
import org.picocontainer.annotations.Inject;
import org.springframework.stereotype.Component;

/**
 * 
 */
@Component
@ManagedService(path = "/device-connect/{param: [a-zA-Z][a-zA-Z_0-9]*}")
@Singleton
public class DeviceVisualizationRealTimeService {

	public static final String PATH = "/device-connect/";
	
	@PathParam("param")
    private String paramName;

    @Inject
    private AtmosphereResourceFactory resourceFactory;
    
    @Inject
    public static MetaBroadcaster metaBroadcaster;
    
    private static BroadcasterFactory factory;

	private final Log log = LogFactory.getLog(getClass());

	@Get
	public void init(AtmosphereResource resource) {
		if(!resource.isCancelled())
		{
			// Set the character encoding as atmospheres default is not unicode.
			resource.getResponse().setCharacterEncoding(StandardCharsets.UTF_8.name());
			factory = resource.getAtmosphereConfig().getBroadcasterFactory();
		}
	}

	@Ready
	public void onReady(final AtmosphereResource resource) {
		
		log.info("Browser " + resource.uuid() + " connected.");
	}

	@Disconnect
	public void onDisconnect(AtmosphereResourceEvent event) {

		if (event.isCancelled()) {

			log.info("Browser " + event.getResource().uuid() + " unexpectedly disconnected");

		} else if (event.isClosedByClient()) {
			
			log.info("Browser " + event.getResource().uuid() + " closed the connection");
			
			resourceFactory.remove(event.getResource().uuid());
				
		}
		
		System.out.println("onDisconnect triggered unexpectedly...");
	}

	@OnMessage
	public String onMessage(String message){

		return message;
	}

	public static void broadcastCenterMessage(String message){
		if(factory != null){
			factory.lookup(DeviceVisualizationRealTimeService.PATH + "centers", true).broadcast(message);
		}
	}
	
	public static void broadcastBedsByCenter(String centerId, Object message) {
		if(factory != null){
			factory.lookup(DeviceVisualizationRealTimeService.PATH + centerId, true).broadcast(message);
		}
		
	}

}