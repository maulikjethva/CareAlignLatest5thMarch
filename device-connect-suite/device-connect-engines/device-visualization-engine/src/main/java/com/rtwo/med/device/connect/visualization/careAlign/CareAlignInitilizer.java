package com.rtwo.med.device.connect.visualization.careAlign;

import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class CareAlignInitilizer {

	private CareAlignDB objCareAlignDB = new CareAlignDB();
	private CareAlignUser objUser = new CareAlignUser();
	private CareAlignPatientInfo objPatient = new CareAlignPatientInfo();
	private CareAlignCaseDetails objCase = new CareAlignCaseDetails();

	@RequestMapping(value = "initDB", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean Connect()
	{
		objCareAlignDB.connectDB();
		objUser.setDB(objCareAlignDB);
		objPatient.setObjDB(objCareAlignDB);
		objCase.setObjDB(objCareAlignDB);
		return true;
	}
	
	public void Disconnect()
	{
		objCareAlignDB.closeDB();
	}
	
	@RequestMapping(value = "authenticate-credential", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean authenticateUser(String userName, String passWord)
	{
//		System.out.println(objUser.getStrUserName());
		objUser.authenticateUser(userName, passWord);
//		return objUser;
		return objUser.getStrUserName() == userName;
	}
	@RequestMapping(value = "register-user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean registerUser(CareAlignUser obj)
	{
		return objUser.registerNewUser(obj);
	}
	
	public void getUserDetails(String userName, String passWord)
	{
		objUser.getUserDetails(userName, passWord);
	}
	
	public boolean registerNewPatientDemo(String userName, String passWord, String patFName, String patLName, String patAddress, String patEmail, String patPhone, String patCity, String patGender)
	{
		return objPatient.registerNewPatientDemo(userName,passWord, patFName, patLName, patAddress, patEmail, patPhone, patCity, patGender);
		
	}
	
	public String registerNewCase(String userName, String passWord, String patName,String patPhone, String treatment, String splInst)
	{
		return objCase.registerNewCase(userName, passWord, patName, patPhone, treatment, splInst);
				
	}
	
	public Map getUsersObjList(String userName, String passWord)
	{
		return objUser.getUsersObjList(userName, passWord);
	}
	
	public void uploadImage(String path)
	{
		objUser.uploadImage(path);
	}
	
	public void downloadImage(String path)
	{
		objUser.downloadImage(path);
	}
}
