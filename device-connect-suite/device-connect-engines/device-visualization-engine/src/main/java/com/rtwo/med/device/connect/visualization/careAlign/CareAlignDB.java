package com.rtwo.med.device.connect.visualization.careAlign;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class CareAlignDB {

	private static Connection con;
	private static Statement stmt;
	
	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		CareAlignDB.con = con;
	}

	public Statement getStmt() {
		return stmt;
	}

	public void setStmt(Statement stmt) {
		CareAlignDB.stmt = stmt;
	}

	public  void connectDB()
	{
		
		 String server = "localhost";
		 String database = "carealign_db";
		 String uid = "root";
		 String password = "admin";
         String connectionString;
         connectionString = "jdbc:mysql://"+server+":3306/"+database;
         
         try{
         Class.forName("com.mysql.jdbc.Driver");  
		 con=DriverManager.getConnection(connectionString,uid,password);
		 stmt=con.createStatement();  
	     }catch(Exception e){
 			System.out.println(e);} 
	}
	
	public  void closeDB()
	{
		try{
		con.close();
		}catch(Exception e){
			System.out.println(e);
		}
	}
	
	private Map getDetails(int iRole,String userName, String passWord )
	{
		Map<String,Map> mapColl=new HashMap<String,Map>();
		List lstUserSize = new ArrayList();
		String query = "";
		if(iRole == 4)//admin
		{
			query = "SELECT User_ID,First_Name,Last_Name,Role,Mobile_Number,Email_ID,Address,City,Reg_By,Gender,Accept,User_RegDate FROM carealign_db.user_detail where `User_Update_Count` = '0'";
		}
		if(iRole == 1 ||iRole == 3)//practicing Dr or Lab Dr
			query = "SELECT User_ID,First_Name,Last_Name,Role,Mobile_Number,Email_ID,Address,City,Reg_By,Gender,Accept,User_RegDate FROM carealign_db.user_detail where User_Name = '"+userName+"' and User_Password = '"+passWord+"'";
		if(iRole == 2)//Sales Rep
		{
			//get list of practicing Dr ids for a role
			int iSaleRepID = 0;
			try{  
				//Sale Rep ID
				String querySel = "SELECT User_ID FROM carealign_db.user_detail where User_Name = '"+userName+"'";
				ResultSet rs=stmt.executeQuery(querySel);  
				
				while(rs.next())
				{
					iSaleRepID = rs.getInt(1);
					System.out.println(rs.getInt(1));
				}
				}catch(Exception e){
					System.out.println(e);}
		
			query = "SELECT User_ID,First_Name,Last_Name,Role,Mobile_Number,Email_ID,Address,City,Reg_By,Gender,Accept,User_RegDate FROM carealign_db.user_detail where `User_Update_Count` = '0' and `User_Sale_Rep_ID` ="+iSaleRepID;
			try{
				int iSize = 0;
				ResultSet rscount =stmt.executeQuery(query); 
				while(rscount.next()){
					rscount.beforeFirst();
					rscount.last();
					iSize = rscount.getRow();
					System.out.println(iSize);
				}}catch(Exception e)
				{ 
					System.out.println(e);
				}
		}
		try{  
			
			ResultSet rs=stmt.executeQuery(query);  

			int iCounter = 0;
			while(rs.next()){
				Map<String,String> map=new HashMap<String,String>(); 
				int iUserID = rs.getInt(1);
				String userId = Integer.toString(iUserID);
				map.put("UserID", userId);
				map.put("First_Name", rs.getString(2));
				map.put("Last_Name", rs.getString(3));
				int iRle = rs.getInt(4);
				String role = Integer.toString(iRle);
				map.put("Role", role);
				map.put("Mobile_Number", rs.getString(5));
				map.put("Email_ID", rs.getString(6));
				map.put("Address", rs.getString(7));
				map.put("City", rs.getString(8));
				map.put("Reg_By", rs.getString(9));
				map.put("Gender", rs.getString(10));
				int iAccept = rs.getInt(11);
				String accept = Integer.toString(iAccept);
				map.put("Accept", accept);
				map.put("User_RegDate", rs.getString(12));
				mapColl.put(Integer.toString(iCounter), map);
				System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"  "+rs.getInt(4)+"  "+rs.getString(5)+"  "+rs.getString(6)+"  "+rs.getString(7)+"  "+rs.getString(8)+"  "+rs.getString(9)+"  "+rs.getString(10)+"  "+rs.getInt(11)+"  "+rs.getString(12));  
				iCounter++;
			}

			}catch(Exception e){
				System.out.println(e);} 
		
		return mapColl;
	}
	
	public Map getUserDetails(String userName, String passWord)
	{
		//identify user role based on username
		int iRole = 0;
		try{  
			
			String query = "SELECT Role FROM carealign_db.user_detail where User_Name = '"+userName+"' and User_Password = '"+passWord+"'";
			ResultSet rs=stmt.executeQuery(query);  
			while(rs.next()){
				iRole = rs.getInt(1);
				System.out.println(rs.getInt(1));  
			}}catch(Exception e){
			System.out.println(e);}
			return getDetails(iRole,userName,passWord);
	}
	
	public boolean authenticate(String userName, String passWord)
	{
		int counter = 0;
		try{  
			
			String query = "SELECT * FROM carealign_db.user_detail where User_Name = '"+userName+"' and User_Password = '"+passWord+"'";
			ResultSet rs=stmt.executeQuery(query);  
			while(rs.next()){
				counter++;
				System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"  "+rs.getString(4)+"  "+rs.getString(5));  
			}}catch(Exception e){
			System.out.println(e);} 
		if(counter >= 1)
			return true;
		else
			return false;
	}
	
	private boolean verifyUserName(String userName)
	{
		int counter = 0;
		try{  
			String query = "SELECT * FROM carealign_db.user_detail where User_Name = '"+userName+"'";
			ResultSet rs=stmt.executeQuery(query);  
			
			while(rs.next())
			counter++;	
			System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"  "+rs.getString(4)+"  "+rs.getString(5));  
			}catch(Exception e){
			System.out.println(e);} 
		if(counter >= 1)
			return true;
		else
			return false;
	}
	
	public boolean registerUser(String strUserName,String strUserFirstName,String strUserLastName,String strEmailId,String strPhone,String strGender,String strAddress,int iRegBy,String strcity)
	{
		if(verifyUserName(strUserName) == false)
		{
			String strRegDate;
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();
			strRegDate = dateFormat.format(date);
			try{  
				
				String query = "INSERT INTO `carealign_db`.`user_detail` (`First_Name`, `Last_Name`, `Role`, `Mobile_Number`, `Email_ID`, `Address`, `City`, `Reg_By`, `Gender`, `Accept`, `User_Name`, `User_RegDate`) VALUES ('"+strUserFirstName+"', '"+strUserFirstName+"', '1', '"+strPhone+"', '"+strEmailId+"', '"+strAddress+"', '"+strcity+"', '"+iRegBy+"', '"+strGender+"', '0', '"+strUserName+"', ' "+strRegDate+"')";
	
				stmt.executeUpdate(query);  
				}catch(Exception e){
				System.out.println(e);} 
			return true;
		}
		return false;
	}
	
/*	public void getUserList(String userName)
	{
		Map<String,String> map=new HashMap<String,String>();  
		int iRole = 0;
		//This functionality is for admin, sales rep or lab doctor.
		try{  
			//String query = "SELECT * FROM carealign_db.user_detail where `User_Update_Count` = '0'";
			//get the role for the user
			//SELECT Role FROM carealign_db.user_detail where `User_Name` = 'shiv'
			//if role is admin all new user list need to be displayed
			//SELECT Role FROM carealign_db.user_detail where `User_Name` = 'shiv'
			//if sale rep or Lab Doc filter accordingly
			
			String query = "SELECT Role FROM carealign_db.user_detail where User_Name = '"+userName+"'";
			ResultSet rs=stmt.executeQuery(query);  
			while(rs.next())
			{
				iRole = rs.getInt(1);
				System.out.println(rs.getInt(1));  
			}
			}catch(Exception e){
			System.out.println(e);} 
		if(iRole == 4) //admin
		{
			try{
				//String query = "SELECT * FROM carealign_db.user_detail where `User_Update_Count` = '0'";
				String query = "SELECT User_ID,First_Name,Last_Name,Role,Mobile_Number,Email_ID,Address,City,Reg_By,Gender,Accept,User_RegDate FROM carealign_db.user_detail where `User_Update_Count` = '0'";
				ResultSet rs=stmt.executeQuery(query);  
				while(rs.next())
				{
				
					System.out.println(rs.getInt(1));  
				}
			}catch(Exception e){
				
			}
		}
	}*/
	
	public boolean registerUserBySalesRep(String strUserName,String strUserFirstName,String strUserLastName,String strEmailId,String strPhone,String strGender,String strAddress,int iRegBy,String strcity, String strSaleRepUserName)
	{
		if(verifyUserName(strUserName) == false)
		{
			String strRegDate;
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();
			strRegDate = dateFormat.format(date);
			int iPracticingDrID = 0;
			int iSaleRepID = 0;
			try{  
				//Sale Rep ID
				String querySel = "SELECT User_ID FROM carealign_db.user_detail where User_Name = '"+strSaleRepUserName+"'";
				ResultSet rs=stmt.executeQuery(querySel);  
				
				while(rs.next())
				{
					iSaleRepID = rs.getInt(1);
					System.out.println(rs.getInt(1));
				}
				}catch(Exception e){
					System.out.println(e);}
			try{  
				
				String query = "INSERT INTO `carealign_db`.`user_detail` (`First_Name`, `Last_Name`, `Role`, `Mobile_Number`, `Email_ID`, `Address`, `City`, `Reg_By`, `Gender`, `Accept`, `User_Update_Count`, `User_Name`, `User_RegDate`, `User_Sales_Rep`, `User_Sale_Rep_ID`) VALUES ('"+strUserFirstName+"', '"+strUserFirstName+"', '1', '"+strPhone+"', '"+strEmailId+"', '"+strAddress+"', '"+strcity+"', '"+iRegBy+"', '"+strGender+"', '0',"+" '0', '"+strUserName+"',' "+strRegDate+"','"+strSaleRepUserName+"','"+iSaleRepID+"')";
				
				stmt.executeUpdate(query);  
				}catch(Exception e){
				System.out.println(e);} 
			try{  
				//Practicing Dr ID use log in user id
				String querySel = "SELECT User_ID FROM carealign_db.user_detail where User_Name = '"+strUserName+"'";
				ResultSet rs=stmt.executeQuery(querySel);  
				
				while(rs.next())
				{
					iPracticingDrID = rs.getInt(1);
					System.out.println(rs.getInt(1));
				}
				}catch(Exception e){
					System.out.println(e);}
			
			try{
				
				String query = "INSERT INTO `carealign_db`.`user_lab_link` (`User_Prac_Dr_ID`, `User_Sale_Rep_ID`) VALUES ('"+iPracticingDrID+"', '"+iSaleRepID+"')";

				stmt.executeUpdate(query);  
				}catch(Exception e){
				System.out.println(e);} 
			return true;
		}
		return false;
	}
	
	boolean registerNewPatientDemo(String userName, String passWord, String patFName, String patLName, String patAddress, String patEmail, String patPhone, String patCity, String patGender)
	{
		int iPracticingDrID = 0;
		int iPatID = 0;
		//check if the patient already exists  based on firstName and phone number
		try{  
			String query = "SELECT User_ID FROM carealign_db.user_detail where User_Name = = '"+userName+"' and User_Password = '"+passWord+"'";
			ResultSet rs=stmt.executeQuery(query);  
			
			while(rs.next())
				iPracticingDrID = rs.getInt(1);
			System.out.println(rs.getInt(1));  
			}catch(Exception e){
			System.out.println(e);} 
		try{  
			String query = "SELECT `Patient_Detail_ID` FROM carealign_db.patient_detail where `Patient_First_Name` = '"+patFName+"' and `Patient_Phone` = '"+patPhone+"'";
			ResultSet rs=stmt.executeQuery(query);  
			
			while(rs.next())
				iPatID = rs.getInt(1);
			System.out.println(rs.getInt(1));  
			}catch(Exception e){
			System.out.println(e);} 
		if(iPatID > 0)
			return false; // patient already present
		try{
			String query = "INSERT INTO `carealign_db`.`patient_detail` (`Patient_First_Name`, `Patient_Last_Name`, `Patient_Address`, `Patient_EMail`, `Patient_Phone`, `Patient_Gender`, `Patient_City`, `user_ID`) VALUES ('patFName', 'patLName', 'patAddress', 'patEmail', 'patPhone', 'patGender', 'patCity', '"+iPracticingDrID+"')";
			
			stmt.executeUpdate(query);  
		}catch(Exception e){
		System.out.println(e);}
		return true;
	}
	
	public Map getPatientDemoDetails(String patFName,String patPhone)
	{
		Map<String,String> map=new HashMap<String,String>(); 
		try{  
			String query = "SELECT Patient_First_Name, Patient_Last_Name,Patient_Address,Patient_EMail,Patient_Phone,Patient_Gender,Patient_City FROM carealign_db.patient_detail where `Patient_First_Name` ='"+patFName+"' and `Patient_Phone` = '"+patPhone+"'";
			ResultSet rs=stmt.executeQuery(query);  
			
			while(rs.next()){
				map.put("FirstName", rs.getString(1));
				map.put("LastName", rs.getString(2));
				map.put("Address", rs.getString(3));
				map.put("Email", rs.getString(4));
				map.put("Phone", rs.getString(5));
				map.put("Gender", rs.getString(6));
				map.put("City", rs.getString(7));
				System.out.println(rs.getInt(1));  
			}
			}catch(Exception e){
			System.out.println(e);}
		return map;
	}
	
	public String registerNewCase(String userName, String passWord, String patName,String patPhone, String treatment, String splInst)
	{
		int iPracticingDrID = 0;
		int iSaleRep = 0;
		int iPatID = 0;
		String strUserFName = "";
		try{  
			String query = "SELECT User_ID, User_Sale_Rep_ID, First_Name FROM carealign_db.user_detail where User_Name = '"+userName+"' and User_Password = '"+passWord+"'";
			ResultSet rs=stmt.executeQuery(query);  
			
			while(rs.next())
			{
				iPracticingDrID = rs.getInt(1);
				iSaleRep = rs.getInt(2);
				strUserFName = rs.getString(3);
			System.out.println(rs.getInt(1));  
			}
			}catch(Exception e){
			System.out.println(e);} 
		
		try{  
			String query = "SELECT Patient_Detail_ID FROM carealign_db.patient_detail where Patient_First_Name = '"+patName+"' and Patient_Phone = '"+patPhone+"'";
			ResultSet rs=stmt.executeQuery(query);  
			
			while(rs.next())
			{
				iPatID = rs.getInt(1);

			System.out.println(rs.getInt(1));  
			}
			}catch(Exception e){
			System.out.println(e);} 
		
		//generate unique case number
		//first three chars of practcing Dr name + unique random number + first three chars of patient name
		String strCaseNum = (strUserFName.substring(0,3)).toUpperCase()+"#"+ (ThreadLocalRandom.current().nextInt( Integer.MAX_VALUE)+1)+"#"+(patName.substring(0,3)).toUpperCase();
		try{
		String query = "INSERT INTO `carealign_db`.`case_detail` (`Case_Number`, `Treatment_Objective`, `Special_Instructions`, `Case_Accepted`, `Practicing_Doctor_ID`, `Rep_ID`, `Patient_Detail_ID`) VALUES ('"+strCaseNum+"', '"+treatment+"', '"+splInst+"', '0', '"+iPracticingDrID+"', '"+iSaleRep+"', '"+iPatID+"')";
		stmt.executeUpdate(query);  
		}catch(Exception e){
			strCaseNum = "";
			System.out.println(e);}
		return strCaseNum;
	}
	
	public void uploadImage(FileInputStream image,File file,String name)
	{
		try{
			PreparedStatement ps=con.prepareStatement("insert into image_extraoral (Image_Extraoral_Name,Image_Extraoral) values(?,?)"); 
			ps.setString(1,"image 1");
			ps.setBinaryStream(2,image,(int)file.length());
            ps.executeUpdate();
       
			
		}catch(Exception e)
		{
			
		}
	}
	
	public void downloadImage(String path)
	{
		File file=new File(path);
        FileOutputStream fos;
        
        try
        {
	        fos =new FileOutputStream(file);
	        byte b[];
	        Blob blob;
	        
	        PreparedStatement ps=con.prepareStatement("select * from image_extraoral where Image_extraoral_ID = 2"); 
	        ResultSet rs=ps.executeQuery();
	        
	        while(rs.next()){
	            blob=rs.getBlob("Image_Extraoral");
	            b=blob.getBytes(1,(int)blob.length());
	            fos.write(b);}
	        
	        fos.close();
        }catch(Exception e){
        	System.out.println(e);
        }
        
	}
	

}
