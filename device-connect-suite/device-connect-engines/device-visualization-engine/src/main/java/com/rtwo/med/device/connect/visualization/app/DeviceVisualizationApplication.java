/**
 * Copyright (c) 2015 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.rtwo.med.device.connect.visualization.app;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.rtwo.med.device.connect.visualization.careAlign.CareAlignCaseDetails;
import com.rtwo.med.device.connect.visualization.careAlign.CareAlignDB;
import com.rtwo.med.device.connect.visualization.careAlign.CareAlignPatientInfo;
import com.rtwo.med.device.connect.visualization.careAlign.CareAlignUser;

/**
 * This class is the staring point of any {@link SpringBootApplication}. NICU
 * Connect will be invoked from here.
 */
@SpringBootApplication
@ComponentScan(basePackages = { 
		"com.rtwo.med.device.connect.visualization.service",
		"com.rtwo.med.device.connect.visualization.config",
		"com.rtwo.med.device.connect.visualization.login",
		"com.rtwo.med.device.connect.visualization.dentalEMR",
		"com.rtwo.med.device.connect.visualization.user",
		"com.rtwo.med.device.connect.visualization.careAlign"
		})
@EnableMongoRepositories(basePackages = "com.rtwo.med.device.connect.mongo.dal.repository")
public class DeviceVisualizationApplication implements CommandLineRunner {
	
//	private static CareAlignDB objCareAlignDB = new CareAlignDB();
//	private static CareAlignUser objUser = new CareAlignUser();
//	private static CareAlignPatientInfo objPatient = new CareAlignPatientInfo();
//	private static CareAlignCaseDetails objCase = new CareAlignCaseDetails();

//	@Autowired
//	private IMessageHandler messageHandle;

	/**
	 * The main method responsible to invoke the
	 * {@link DeviceVisualizationApplication}.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// Start Spring Boot Container.
		SpringApplication.run(DeviceVisualizationApplication.class, args).getEnvironment().getSystemProperties();
		
//		
//		System.out.println("Session");
//		objCareAlignDB.connectDB();
//		objUser.setDB(objCareAlignDB);
//		objPatient.setObjDB(objCareAlignDB);
//		objCase.setObjDB(objCareAlignDB);

	}

	@Override
	public void run(String... args) {
		Thread thread = new Thread();
		thread.start();
	}

}
