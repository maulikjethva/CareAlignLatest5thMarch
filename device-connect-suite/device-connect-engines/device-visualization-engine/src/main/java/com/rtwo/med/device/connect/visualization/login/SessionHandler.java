/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

package com.rtwo.med.device.connect.visualization.login;

import java.sql.Date;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionHandler implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		HttpSession session=se.getSession();
		System.out.println("session time:"+getTime());
		System.out.println("SessionID="+session.getId()+" "+"MaxInactiveInterval="+session.getMaxInactiveInterval());
		System.out.println("session created");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSession session=se.getSession();
		System.out.println("DestroyedID="+getTime()+session.getId());
		System.out.println("session destroyed");
		
	}
	
	private String getTime(){
		return new Date(System.currentTimeMillis()).toString();
		
	}

}
