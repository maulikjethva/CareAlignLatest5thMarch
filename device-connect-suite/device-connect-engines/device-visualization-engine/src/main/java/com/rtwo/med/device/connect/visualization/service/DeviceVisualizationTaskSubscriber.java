///**
// * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
// *
// * The copyright to the computer software herein is the property of
// * RTWO Healthcare Solutions. The software may be used and/or copied only
// * with the written permission of RTWO Healthcare Solutions or in accordance
// * with the terms and conditions stipulated in the agreement/contract
// * under which the software has been supplied.
// */
//
//package com.rtwo.med.device.connect.visualization.service;
//
//import java.util.concurrent.ConcurrentMap;
//
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import com.rtwo.med.device.connect.cache.CacheKey;
//import com.rtwo.med.device.connect.cache.CacheManager;
//import com.rtwo.med.device.connect.dto.CenterDTO;
//import com.rtwo.med.device.connect.dto.SupervisorDTO;
//
///**
// * Checks if there is no activity from the center.
// *
// * @author bhuvan
// *
// */
////TODO Need to check if no activity from the bed as well.
//public class DeviceVisualizationTaskSubscriber implements Runnable {
//    @Override
//    public void run() {
//    	ConcurrentMap<String, CenterDTO> centersCache = CacheManager.INSTANCE.getValue(CacheKey.CENTERS_CACHE, CacheKey.CENTERS_CACHE);
//    	ConcurrentMap<String, SupervisorDTO> supervisorCache = CacheManager.INSTANCE.getValue(CacheKey.SUPERVISOR_CACHE, CacheKey.SUPERVISOR_CACHE);
//    	
//    	if(centersCache != null && supervisorCache != null){
//    		for(CenterDTO center : centersCache.values()) {
//				long lastUpdt = center.getLastUpdated();
//				long now = System.currentTimeMillis();
//				
//				long diff = now - lastUpdt;
//				//if exceeds 5 minutes
//				if(diff > 300000) {
//					
//					SupervisorDTO supervisor = supervisorCache.remove(center.getId());
//					CacheManager.INSTANCE.putToCache(CacheKey.SUPERVISOR_CACHE, CacheKey.SUPERVISOR_CACHE, supervisorCache);
//					
//					centersCache.remove(center.getId());
//					CacheManager.INSTANCE.putToCache(CacheKey.CENTERS_CACHE, CacheKey.CENTERS_CACHE, centersCache);
//					
//					Gson json = new GsonBuilder().create();
//					supervisor.getBeds().get(0).setBedId(null);
//					String message = json.toJson(supervisor);
//					DeviceVisualizationRealTimeService.broadcastBedsByCenter(center.getId(), message);
//				}
//    		}
//    	}
//    }
//}