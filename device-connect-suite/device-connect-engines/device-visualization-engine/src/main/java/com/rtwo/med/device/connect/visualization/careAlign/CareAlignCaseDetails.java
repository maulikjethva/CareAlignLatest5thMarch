package com.rtwo.med.device.connect.visualization.careAlign;

public class CareAlignCaseDetails {
	
	private String strCaseNumber;
	private String strCaseObjective;
	private String strSpecialInstrections;
	private CareAlignDB objDB;
	
	public CareAlignDB getObjDB() {
		return objDB;
	}
	public void setObjDB(CareAlignDB objDB) {
		this.objDB = objDB;
	}
	public String getStrCaseNumber() {
		return strCaseNumber;
	}
	public void setStrCaseNumber(String strCaseNumber) {
		this.strCaseNumber = strCaseNumber;
	}
	public String getStrCaseObjective() {
		return strCaseObjective;
	}
	public void setStrCaseObjective(String strCaseObjective) {
		this.strCaseObjective = strCaseObjective;
	}
	public String getStrSpecialInstrections() {
		return strSpecialInstrections;
	}
	public void setStrSpecialInstrections(String strSpecialInstrections) {
		this.strSpecialInstrections = strSpecialInstrections;
	}
	
	public String registerNewCase(String userName, String passWord, String patName,String patPhone, String treatment, String splInst)
	{
		return objDB.registerNewCase(userName, passWord, patName, patPhone, treatment, splInst);
	}
}
