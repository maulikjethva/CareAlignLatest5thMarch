package com.rtwo.med.device.connect.visualization.dentalEMR;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rtwo.med.device.connect.dto.ConsultationDTO;
import com.rtwo.med.device.connect.dto.PatientsDTO;
import com.rtwo.med.device.connect.dto.ValueRefDTO;
import com.rtwo.med.device.connect.dto.ValuesDTO;
import com.rtwo.med.device.connect.mongo.dal.entities.Categoryvalue;
import com.rtwo.med.device.connect.mongo.dal.entities.Consultationexamination;
import com.rtwo.med.device.connect.mongo.dal.entities.Consultationtreatment;
import com.rtwo.med.device.connect.mongo.dal.entities.Patients;
import com.rtwo.med.device.connect.mongo.dal.entities.Specialtyvalue;
import com.rtwo.med.device.connect.mongo.dal.entities.Statusvalue;
import com.rtwo.med.device.connect.mongo.dal.entities.Surfacevalue;
import com.rtwo.med.device.connect.mongo.dal.entities.Treatmentvalue;
import com.rtwo.med.device.connect.mongo.dal.repository.CategoryValueRepository;
import com.rtwo.med.device.connect.mongo.dal.repository.ConsultationExaminationRepository;
import com.rtwo.med.device.connect.mongo.dal.repository.ConsultationTreatmentRepository;
import com.rtwo.med.device.connect.mongo.dal.repository.PatientRepository;
import com.rtwo.med.device.connect.mongo.dal.repository.SpecialtyValueRepository;
import com.rtwo.med.device.connect.mongo.dal.repository.StatusValueRepository;
import com.rtwo.med.device.connect.mongo.dal.repository.SurfaceValueRepository;
import com.rtwo.med.device.connect.mongo.dal.repository.TreatmentValueRepository;

@RestController
public class DentalEMRController {
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired
	private SurfaceValueRepository surfaceRepository;
	
	@Autowired
	private StatusValueRepository statusRepository;
	
	@Autowired
	private SpecialtyValueRepository specialityRepository;
	
	@Autowired
	private CategoryValueRepository categoryRepository;
	
	@Autowired
	private TreatmentValueRepository treatmentRepository;
	
	@Autowired
	private ConsultationExaminationRepository consultExamRepository;
	
	@Autowired
	private ConsultationTreatmentRepository consultTreatmentRepository;
	
	/*
	 * Getting Patient information base on patient name
	 * @Nishanth
	 */
	@RequestMapping(value = "get-patient-info-by-name", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<PatientsDTO> getPatientInfo(@RequestParam("patientName") String patientName) {
		
		List<PatientsDTO> patientDTOList = new ArrayList<>();
		List<Patients> patientList = this.patientRepository.findAll();
		
		for(Patients patient: patientList) {
			
			if(patient.getPatientName().equals(patientName)) {
				PatientsDTO patientDTO = new PatientsDTO();
				
				patientDTO.setPatientGender(patient.getPatientGender());
				patientDTO.setPatientSelectedMedicalCondition(patient.getPatientSelectedMedicalCondition());
				patientDTO.setStartTime(patient.getStartTime());
				patientDTO.setEndTime(patient.getEndTime());
				patientDTO.setHospitalName(patient.getHospitalName());
				patientDTO.setDentist_id(patient.getDentist_id());
				patientDTO.setDentist(patient.getDentist());
				patientDTO.setTreatment(patient.getTreatment());
				patientDTO.setPatientPlace(patient.getPatientPlace());
				patientDTO.setPatientName(patient.getPatientName());
				patientDTO.setPatientAge(patient.getPatientAge());
				patientDTO.setEmailId(patient.getEmailId());
				patientDTO.setContact(patient.getContact());
				patientDTO.setCreated(patient.getCreated());
				
				patientDTOList.add(patientDTO);
			}
			
		}
		
		return patientDTOList;
	}
	
	/*
	 * save temp
	 * @nishanth
	 */
//	@RequestMapping(value = "save-specialty-info", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//	public @ResponseBody boolean saveValues(@RequestBody List<ValuesDTO> valuesDTO) {
//		
//		System.out.println("TRIGGERED");
//		
//		List<Specialtyvalue> SpecialityList = new ArrayList<>();
//		
//		for(ValuesDTO values: valuesDTO ) {
//			Specialtyvalue status = new Specialtyvalue();
//			System.out.println(values.getValue());
//			status.setValue(values.getValue());
//			SpecialityList.add(status);
//		}
//		System.out.println("HERE");
//		return this.specialityRepository.save(SpecialityList)!= null;
//	}
	
	/*
	 * Getting Surface value from database
	 * @Nishanth
	 */
	@RequestMapping(value = "get-surface-info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<ValuesDTO> getSurfaceValues() {
		
		List<ValuesDTO> valuesDTOList = new ArrayList<>();
		List<Surfacevalue> surfaceList = this.surfaceRepository.findAll();
		
		for (Surfacevalue surfaceValues : surfaceList) {
			
			ValuesDTO valuesDTO = new ValuesDTO();
			valuesDTO.setValue(surfaceValues.getValue());
			valuesDTOList.add(valuesDTO);
		}
		
		return valuesDTOList;
	}
	
	/*
	 * Getting Status value from database
	 */
	@RequestMapping(value = "get-status-info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<ValuesDTO> getStatusValues() {
		
		List<ValuesDTO> valuesDTOList = new ArrayList<>();
		List<Statusvalue> statusList = this.statusRepository.findAll();
		
		for (Statusvalue statusValues : statusList) {
			
			ValuesDTO valuesDTO = new ValuesDTO();
			valuesDTO.setValue(statusValues.getValue());
			valuesDTOList.add(valuesDTO);
		}
		
		return valuesDTOList;
	}
	
	/*
	 * Getting Speciality value from database
	 */
	@RequestMapping(value = "get-specialty-info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<ValuesDTO> getSpecialityValues() {
		
		List<ValuesDTO> valuesDTOList = new ArrayList<>();
		List<Specialtyvalue> specialityList = this.specialityRepository.findAll();
		
		for (Specialtyvalue specialityValues : specialityList) {
			
			ValuesDTO valuesDTO = new ValuesDTO();
			valuesDTO.setValue(specialityValues.getValue());
			valuesDTOList.add(valuesDTO);
		}
		
		return valuesDTOList;
	}
	
	/*
	 * Getting Category value from database
	 */
	@RequestMapping(value = "get-category-info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<ValueRefDTO> getCategoryValues(@RequestParam("data") String data) {
		
		List<ValueRefDTO> valuesRefDTOList = new ArrayList<>();
		List<Categoryvalue> categoryList = this.categoryRepository.findAll();
		
		for (Categoryvalue categoryValues : categoryList) {
			
			if(data.equalsIgnoreCase(categoryValues.getRef())) {				
				ValueRefDTO valuesRefDTO = new ValueRefDTO();
				valuesRefDTO.setValue(categoryValues.getValue());
				valuesRefDTO.setRef(categoryValues.getRef());
				valuesRefDTOList.add(valuesRefDTO);
			}
			
		}
		
		return valuesRefDTOList;
	}
	
	/*
	 * Getting Treatment value from database
	 */
	@RequestMapping(value = "get-treatment-info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<ValueRefDTO> getTreatmentValues(@RequestParam("data") String data) {
		
		List<ValueRefDTO> valuesRefDTOList = new ArrayList<>();
		List<Treatmentvalue> treatmentList = this.treatmentRepository.findAll();
		
		for (Treatmentvalue treatmentValues : treatmentList) {
			
			if(data.equalsIgnoreCase(treatmentValues.getRef())) {	
				ValueRefDTO valuesRefDTO = new ValueRefDTO();
				valuesRefDTO.setValue(treatmentValues.getValue());
				valuesRefDTO.setRef(treatmentValues.getRef());
				valuesRefDTOList.add(valuesRefDTO);
			}
			
		}
		
		return valuesRefDTOList;
	}
	
	@RequestMapping(value = "save-consultation-info", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody boolean saveConsultation(@RequestBody List<ConsultationDTO> consultDTO) {
		
		List<Consultationexamination> examList = new ArrayList<>();
		List<Consultationtreatment> treatmentList = new ArrayList<>();
		
		String consultMethod = "";
		
		for(ConsultationDTO consult: consultDTO) {
			if(consult.getAmount() == "") {
				consultMethod = "examination";
				Consultationexamination exam = new Consultationexamination();
				exam.setCategory(consult.getCategory());
				exam.setPerformedBy(consult.getPerformedBy());
				exam.setPerformedDate(consult.getPerformedDate());
				exam.setPlannedDate(consult.getPlannedDate());
				exam.setRemark(consult.getRemark());
				exam.setSpecialty(consult.getSpecialty());
				exam.setStatus(consult.getStatus());
				exam.setSurface(consult.getStatus());
				exam.setTeethNo(consult.getTeethNo());
				exam.setTreatment(consult.getTreatment());
				
				examList.add(exam);
			}
			else {
				consultMethod = "treatment";
				Consultationtreatment treatment = new Consultationtreatment();
				treatment.setAmount(consult.getAmount());
				treatment.setCategory(consult.getCategory());
				treatment.setPerformedBy(consult.getPerformedBy());
				treatment.setPerformedDate(consult.getPerformedDate());
				treatment.setPlannedDate(consult.getPlannedDate());
				treatment.setRemark(consult.getRemark());
				treatment.setSpecialty(consult.getSpecialty());
				treatment.setStatus(consult.getStatus());
				treatment.setSurface(consult.getSurface());
				treatment.setTeethNo(consult.getTeethNo());
				treatment.setTreatment(consult.getTreatment());
				
				treatmentList.add(treatment);
			}
		}
		
		if(consultMethod == "examination") {
			return this.consultExamRepository.save(examList) != null;
		}
		else {
			return this.consultTreatmentRepository.save(treatmentList) != null;
		}
	}


}
