/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

package com.rtwo.med.device.connect.visualization.login;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;


@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public  class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	MongoDBAuthenticationProvider mongoDBAuthenticationProvider;


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().and().authorizeRequests().antMatchers("/**").permitAll().anyRequest()
		.authenticated().and().csrf().csrfTokenRepository(csrfTokenRepository()).and()
		.addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class);

	}

	/**
	 * Send user credentials to validate against the database
	 */
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(mongoDBAuthenticationProvider);		
	}

	/**
	 * 
	 * The class will protecting against CSRF attacks
	 *
	 */
	public class CsrfHeaderFilter extends OncePerRequestFilter{

		@Override
		protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
				FilterChain filterChain) throws ServletException, IOException {

			CsrfToken csrf=(CsrfToken) request.getAttribute(CsrfToken.class.getName());
			if(csrf != null){
				Cookie cookie=WebUtils.getCookie(request, "XSRF-TOKEN");
				String token=csrf.getToken();
				if(cookie == null || token != null && !token.equals(cookie.getValue())){
					cookie=new Cookie("XSRF-TOKEN", token);
					cookie.setPath("/");
					response.addCookie(cookie);
				}

			}
			filterChain.doFilter(request, response);
		}
	}

	private CsrfTokenRepository csrfTokenRepository(){
		HttpSessionCsrfTokenRepository repository=new HttpSessionCsrfTokenRepository();
		repository.setHeaderName("X-XSRF-TOKEN");
		return repository;
	}
}
