/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

package com.rtwo.med.device.connect.visualization.user;

import java.util.ArrayList;
import java.util.List;

/*import java.util.List;*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rtwo.med.device.connect.dto.UserEntityDTO;
import com.rtwo.med.device.connect.dto.UserPasswordDTO;
import com.rtwo.med.device.connect.mongo.dal.entities.UserEntity;
import com.rtwo.med.device.connect.mongo.dal.repository.UserEntityRepository;
import com.rtwo.med.device.connect.util.crypto.IdUtil;

/**
 * 
 * UserManagementController class is used for to save, retrieve, update, and
 * delete the user details
 *
 */
@RestController
public class UserController {
	
	@Autowired
	private UserEntityRepository userRepository;

	@RequestMapping(value = "save-user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	/**
	 * Get the user details from the UI and save it in the database
	 */
	public @ResponseBody boolean save(@RequestBody UserEntityDTO userEntityDTO) {

		UserEntity userEntity = new UserEntity();
		userEntity.setUsername(userEntityDTO.getLogId());
		userEntity.setPassword(IdUtil.encrypt(userEntityDTO.getPassword()));
		userEntity.setFirstName(userEntityDTO.getfName());
		userEntity.setLastName(userEntityDTO.getlName());
		userEntity.setEmail(userEntityDTO.getEmail());
		userEntity.setPhone(userEntityDTO.getPhone());
		userEntity.setRoles(userEntityDTO.getRoles());
		userEntity.setPatientModificationAllowed(userEntityDTO.getPatientModificationAllowed());
		userEntity.setBedModificationAllowed(userEntityDTO.getBedModificationAllowed());

		if (this.userRepository.findByusername(userEntity.getUsername()) != null) {

			return false;
		}
		
		return (this.userRepository.save(userEntity) != null);

	}

	/**
	 * 
	 * Retrieve the user details from the database and send to the UI
	 */
	@RequestMapping(value = "get-user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)

	public @ResponseBody List<UserEntityDTO> getAllUser() {

		List<UserEntityDTO> UserDTOList = new ArrayList<>();
		List<UserEntity> userInfoList = this.userRepository.findAll();
		
		for (UserEntity userInfo : userInfoList) {
			
			UserEntityDTO udto = new UserEntityDTO();
			
			udto.setLogId(userInfo.getUsername());
			udto.setPassword(IdUtil.decrypt(userInfo.getPassword()));
			udto.setfName(userInfo.getFirstName());
			udto.setlName(userInfo.getLastName());
			udto.setEmail(userInfo.getEmail());
			udto.setPhone(userInfo.getPhone());
			udto.setRoles(userInfo.getRoles());
			udto.setPatientModificationAllowed(userInfo.isPatientModificationAllowed());
			udto.setBedModificationAllowed(userInfo.isBedModificationAllowed());
			
			UserDTOList.add(udto);
		}
		
		return UserDTOList;
	}

	/**
	 * 
	 * Update user details in the database which is selected and updated by the
	 * user from UI
	 * 
	 */
	@RequestMapping(value = "update-user", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)

	public @ResponseBody boolean update(@RequestBody UserEntityDTO userEntityDTO) {

		UserEntity userEntity = new UserEntity();
		
		userEntity.setUsername(userEntityDTO.getLogId());
		userEntity.setPassword(IdUtil.encrypt(userEntityDTO.getPassword()));
		userEntity.setFirstName(userEntityDTO.getfName());
		userEntity.setLastName(userEntityDTO.getlName());
		userEntity.setEmail(userEntityDTO.getEmail());
		userEntity.setPhone(userEntityDTO.getPhone());
		userEntity.setRoles(userEntityDTO.getRoles());
		userEntity.setPatientModificationAllowed(userEntityDTO.getPatientModificationAllowed());
		userEntity.setBedModificationAllowed(userEntityDTO.getBedModificationAllowed());

		return this.userRepository.save(userEntity) != null;

	}

	/**
	 * 
	 * Delete the user details from the database which is selected by the user
	 * in UI
	 */
	@RequestMapping(value = "delete-user", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)

	public @ResponseBody void delete(@RequestBody UserEntityDTO userEntityDTO) {
		
		UserEntity userEntity = new UserEntity();
		userEntity.setUsername(userEntityDTO.getLogId());

		this.userRepository.delete(userEntity.getUsername());
	}

	/**
	 * 
	 * Retrieve the user details from the database and send to the UI
	 */
	@RequestMapping(value = "get-profile-user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody UserEntityDTO getUserProfile(@RequestParam("profileId") String profileId) {

		UserEntity userInfo = this.userRepository.findByusername(profileId);
		UserEntityDTO udto = new UserEntityDTO();
		
		udto.setLogId(userInfo.getUsername());
		udto.setPassword(IdUtil.decrypt(userInfo.getPassword()));
		udto.setfName(userInfo.getFirstName());
		udto.setlName(userInfo.getLastName());
		udto.setEmail(userInfo.getEmail());
		udto.setPhone(userInfo.getPhone());
		udto.setBedModificationAllowed(userInfo.isBedModificationAllowed());
		udto.setPatientModificationAllowed(userInfo.isPatientModificationAllowed());
		
		return udto;
	}

	/**
	 * Get the user details from the UI and save it in the database
	 */
	@RequestMapping(value = "save-profile-user", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody boolean saveUserProfile(@RequestBody UserEntityDTO userEntityDTO) {

		UserEntity prevData = this.userRepository.findByusername(userEntityDTO.getLogId());

		UserEntity userEntity = new UserEntity();
		
		userEntity.setUsername(userEntityDTO.getLogId());
		userEntity.setPassword(IdUtil.encrypt(prevData.getPassword()));

		userEntity.setFirstName(userEntityDTO.getfName());
		userEntity.setLastName(userEntityDTO.getlName());
		userEntity.setEmail(userEntityDTO.getEmail());
		userEntity.setPhone(userEntityDTO.getPhone());

		userEntity.setRoles(prevData.getRoles());

		if (this.userRepository.findByusername(userEntityDTO.getLogId()) != null) {
			return (this.userRepository.save(userEntity) != null);
		}

		return false;

	}

	/**
	 * Get the user details from the UI and save it in the database
	 */
	@RequestMapping(value = "save-profile-password", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody boolean saveUserProfilePassword(@RequestBody UserPasswordDTO userPassword) {

		UserEntity prevData = this.userRepository.findByusername(userPassword.getLogId());

		if (prevData.getPassword().equals(IdUtil.encrypt(userPassword.getOldPassword()))) {

			prevData.setPassword(IdUtil.encrypt(userPassword.getNewPassword()));

			if (this.userRepository.findByusername(userPassword.getLogId()) != null) {
				return (this.userRepository.save(prevData) != null);
			}
		}

		return false;

	}
}
