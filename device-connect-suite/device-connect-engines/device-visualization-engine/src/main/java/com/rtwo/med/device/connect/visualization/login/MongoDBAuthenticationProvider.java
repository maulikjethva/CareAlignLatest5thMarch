/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */


package com.rtwo.med.device.connect.visualization.login;


import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.rtwo.med.device.connect.mongo.dal.entities.UserEntity;
import com.rtwo.med.device.connect.mongo.dal.repository.UserEntityRepository;
import com.rtwo.med.device.connect.util.crypto.IdUtil;

//import com.rtwo.med.device.nicu.connect.usermanual.usercontroller.UserEntity;
//import com.rtwo.med.device.nicu.connect.usermanual.usercontroller.UserEntityRepository;

/**
 * 
 * Returns the Authenticated user
 *
 */
@Component
public class MongoDBAuthenticationProvider implements AuthenticationProvider   {

	@Autowired
	UserEntityRepository userRepository;

	/*
	 * It accpts the user credential and validating against the database
	 * 
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String name=(String) authentication.getPrincipal();
		String password=IdUtil.encrypt((String) authentication.getCredentials());

		List<UserGrantedAuthority> userGrantedAuthorityList=new ArrayList<>();
		UserEntity userInfo=this.userRepository.findByUsernameAndPassword(name, password);
		if(userInfo != null){
			List<String> role=userInfo.getRoles();
			for (String userRole : role) {
				UserGrantedAuthority userGrantedAuthority=new UserGrantedAuthority(userRole);
				userGrantedAuthorityList.add(userGrantedAuthority);
			}
			return new UsernamePasswordAuthenticationToken(name, password,userGrantedAuthorityList);	
		}
		return null;	
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}


