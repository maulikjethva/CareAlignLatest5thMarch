/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

define(['angular', 'underscore'], function (angular, _) {
	'use strict';
	return angular.module('ui.app.modules.header.controller', [])
		.controller('HeaderController', ['$scope', 'HttpService', '$location', '$rootScope', 'ActiveContextService', 'ActiveUserInfoService', function ($scope, HttpService, $location, $rootScope, ActiveContextService, ActiveUserInfoService) {

			$scope.logout = function () {

				HttpService.create('logout', {},
					function (response) {
						$rootScope.authenticated = false;
						$location.path('/');
					}, function (error) {

					});
			};
			
//			nishanth
			$scope.$on('load_user_view', function (event) {
				$scope.guestRoles = false;
				$scope.userName = ActiveUserInfoService.getActiveUserFname() + ' ' + ActiveUserInfoService.getActiveUserLname();
				// $scope.roles = ActiveUserInfoService.getUserRoles();
				_.find(ActiveUserInfoService.getUserRoles(), function (roles) {
					if (roles === 'GUEST') {
						$scope.guestRoles = true;
					}
				});
            });
			
		}]);

});