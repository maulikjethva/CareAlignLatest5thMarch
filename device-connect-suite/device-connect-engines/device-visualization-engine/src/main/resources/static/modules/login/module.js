/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

define(['angular',
'modules/login/controllers/AuthenticationController'
    ], function(angular) {
        'use strict';
        return angular.module('ui.app.module.login',[
            'ui.app.modules.login.controller'
            ]);
    });