/**
 * Copyright (c) 2015 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

define(['angular',
    'modules/commons/services/active-context-service',
    'modules/commons/services/active-user-info-service',
    'modules/commons/services/http-service',
    'modules/commons/services/toastr-service',
    'modules/commons/channels/real-time-channel',
    'modules/commons/channels/real-time-beds-info-channel',
    'modules/commons/channels/event-bus-factory',
    'modules/commons/channels/notification-channel',
    'modules/commons/channels/device-viewer-channel',
    'modules/commons/directives/header',
    'modules/commons/directives/footer',
    'modules/commons/controllers/header-controller'
], function (angular) {

    'use strict';
    return angular.module('ui.app.module.common', [
        'ui.app.modules.commons.service.active.context',
        'ui.app.modules.commons.service.active.user.info',
        'ui.app.modules.commons.service.toastr.service',
        'ui.app.modules.commons.service.rest',
        'ui.app.modules.commons.channel.realtime',
        'ui.app.modules.commons.channel.realtime.beds',
        'ui.app.modules.commons.channel.event.bus',
        'ui.app.modules.commons.channel.notification',
        'ui.app.modules.commons.channel.device.viewer',
        'ui.modules.commons.directives.header',
        'ui.modules.commons.directives.footer',
        'ui.app.modules.header.controller'
    ]);
});