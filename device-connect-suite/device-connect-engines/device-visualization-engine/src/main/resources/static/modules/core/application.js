/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Starting point of the Application. This is responsible to init all
 * injectables & routes the request based on path configuration.
 */
define([
    'angular',
    'jquery',
    'angularRoute',
    'angularMom',
    'bootstrap',
    'angularBootstrap',
    'underscore',
    'postal',
    'parallel',
    'respond',
    'html5shiv',
    'modules/commons/module',
    'modules/login/module',
    'modules/home/module'
],
    function (angular, $) {
        'use strict';
        return angular.module('nicuConnect', [
            'ngRoute',
            'angularMoment',
            'ui.bootstrap',
            'ui.app.module.common',
            'ui.app.module.home',
            'ui.app.module.login'
        ]).config(['$httpProvider', '$routeProvider', '$sceDelegateProvider', 'HttpServiceProvider', 
            function ($httpProvider, $routeProvider, $sceDelegateProvider, HttpServiceProvider) {

                var domain = window.location;
                var url = domain.protocol + '//' + domain.hostname + ':' + domain.port + '/';//domain.port

                $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

                $routeProvider.when('/', {
                    //Login Routing.

//                    templateUrl: 'modules/home/views/home.html',
//                    controller: 'HomeController',

                    templateUrl: 'modules/login/views/login.html',
                    controller: 'AuthenticationController',
                    title: 'NICU Connect',
                    resolve: {
                        restServiceProvider: [function () {
                            //Set the local Server URL.
                            HttpServiceProvider.setBaseURL(url);
                        }]
                    }
                }).when('/home', {
                    //Home Routing.
                    resolve: {
                        loginRouteCheck: ['$location', '$rootScope', 'HttpService', function ($location, $rootScope, HttpService) {//TODO:What is the use of this??
                            if (!$rootScope.authenticated) {

                                HttpService.create('logout', {},
                                    function (response) {
                                        $location.path('/');
                                    }, function (error) {

                                    });
                            }
                            $rootScope.authenticated = false;
                        }]
                    },
                    templateUrl: 'modules/home/views/home.html',
                    controller: 'HomeController',
                    title: 'NICU Connect'
                }).when('/guest', {
                    //Home Routing.
                    resolve: {
                        loginRouteCheck: ['$location', '$rootScope', 'HttpService', function ($location, $rootScope, HttpService) {//TODO:What is the use of this??
                            if (!$rootScope.authenticated) {

                                HttpService.create('logout', {},
                                    function (response) {
                                        $location.path('/');
                                    }, function (error) {

                                    });
                            }
                            $rootScope.authenticated = false;
                        }]
                    },
                    templateUrl: 'modules/video/views/video-list.html',
                    controller: 'VideoListController',
                    title: 'NICU Connect'
                })
                    .otherwise({ redirectTo: '/' });//TODO: We may need to route to a Error page.*/
            }])
            .run(['$rootScope', function ($rootScope) {

                //Listens & trigger on route changes.
                $rootScope.$on('$routeChangeSuccess', function (event, data) {
                    //set the application title based on the route selection.
                    $rootScope.pageTitle = data.title;
                });
            }]);
    });
