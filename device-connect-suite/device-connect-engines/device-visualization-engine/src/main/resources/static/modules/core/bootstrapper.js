/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * This resource is responsible to configure all require js based configuration
 * as well as manual Angular Bootstrap of this Application.
 */
require.config({
    baseUrl: './',
    waitSeconds: 30, //TODO: Optimize the loading time of the dependencies.
    paths: {
        requirejs: 'dependencies/requirejs/require',
        domready: 'dependencies/requirejs-domready/domReady',

        bootstrap: 'dependencies/bootstrap/dist/js/bootstrap.min',
        jquery: 'dependencies/jquery/dist/jquery.min',
        jqueryUi: 'dependencies/jquery-ui/jquery-ui.min',

        angular: 'dependencies/angular/angular.min',
        angularRoute: 'dependencies/angular-route/angular-route.min',
        angularCookies: 'dependencies/angular-cookies/angular-cookies.min',
        angularResource: 'dependencies/angular-resource/angular-resource.min',
        angularSanitize: 'dependencies/angular-sanitize/angular-sanitize.min',
        angularAnimate: 'dependencies/angular-animate/angular-animate.min',
        angularBootstrap: 'dependencies/angular-bootstrap/ui-bootstrap-tpls.min',

        moment: 'dependencies/moment/min/moment.min',
        angularMom: 'dependencies/angular-moment/angular-moment.min',
        toastr: 'dependencies/toastr/toastr.min',
        atmosphere: 'dependencies/atmosphere/atmosphere.min',
        lodash: 'dependencies/lodash/lodash.min',
        underscore: 'dependencies/underscore/underscore-min',
        postal: 'dependencies/postal/postal.min',
        parallel: 'dependencies/paralleljs/lib/parallel',
        respond: 'dependencies/respond/dest/respond.min',
        html5shiv: 'dependencies/html5shiv/dist/html5shiv.min',
        core: 'modules/core',
        adapter: 'dependencies/adapter.js/adapter',
    },
    shim: {
        angular:{
            deps: ['jquery'],
            exports: 'angular'
        },
        bootstrap: ['jquery'],
        angularRoute: ['angular'],
        angularCookies: ['angular'],
        angularResource: ['angular'],
        angularSanitize: ['angular'],
        angularAnimate: ['angular'],
        angularBootstrap: ['angular', 'bootstrap'],
        angularMom: ['angular', 'moment'],
        
        postal: ['lodash']
    }
});

/**
 * Angular Application Bootstrapping Manually
 * instead of Automatically bootstrapping using ng-app.
 */
require(['angular', 'modules/core/application', 'domready'], function(angular, app, domready){
   'use strict';
    domready(function(){
        angular.bootstrap(document, [app.name]);
    });
});