define(['angular'], function(angular) {
    'use strict';

    return angular.module('ui.app.modules.emr.directive', [])
        .directive('emr', [function () {
            return {
                restrict: 'E',
                replace:true,
                templateUrl:'modules/emr/views/emr.html',
            	controller: 'EMRController',
                scope: true,

                link: function(scope, element, attributes){
                    scope.template = 'modules/emr/views/appointment-registration.html';
                    scope.selectedLi = 'appointment-registration';
                                                            
                    scope.getEmrHtml = function(val){
                        scope.selectedLi = val;
                        scope.template = 'modules/emr/views/'+val+'.html';
                        
                        if(val = 'consultation') {
                        	scope.examinationLoad();
                        }
                    }
                }
            };
        }]);
});