/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Stores all Centers which are sharable to others.
 */
define(['angular', 'postal'], function (angular, postal) {
    return angular.module('ui.app.modules.commons.channel.event.bus', [])
        .factory('EventBusFactory', [function () {

            var _subscriptions = {};

            var publish = function (channel, topic, data) {
                postal.publish({
                    channel: channel,
                    topic: topic,
                    data: data
                });
            };

            var subscribe = function (channel, topic, callback) {
                _subscriptions[channel] = postal.subscribe({
                    channel: channel,
                    topic: topic,
                    callback: callback.bind(this)
                });
            };

            var unsubscribe = function (channel) {
                if(_subscriptions[channel]) {
                    _subscriptions[channel].unsubscribe();
                    delete _subscriptions[channel];
                }
            };

            var unsubscribeAll = function () {
                var self = this;
                for (var subscription in _subscriptions) {
                    this.unsubscribe(subscription);
                }
            };

            return {

                publish: publish,

                subscribe: subscribe,

                unsubscribe: unsubscribe,

                unsubscribeAll: unsubscribe
            };
        }]);
});