/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Center Tile Widget.
 */
define(['angular', 'underscore'], function (angular, _) {
    'use strict';

    return angular.module('ui.modules.home.directives.home.center-tile', [])
        .directive('centerTile', [function () {
            return {
                restrict: 'E',
                replace: true,
                templateUrl: 'modules/home/views/center-tile.html',
                scope: {
                    thisCenter: '='
                },
                link: function (scope, element, attributes) {

                }
            };
        }]);
});