/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Application Footer Widget.
 */
define(['angular'], function(angular){
    'use strict';

    return angular.module('ui.modules.commons.directives.footer', [])
        .directive('appFooter', [function(){
            return{
                restrict: 'E',
                replace: true,
                templateUrl: 'modules/commons/views/footer.html',
                controller: '',//TODO: Header Controller need to be implemented.
                scope: {
                    //TODO: Scope vars should be defined.
                },
                link: function(scope, element, attributes){
                    //TODO: We may need later.
                }
            };
        }]);
});