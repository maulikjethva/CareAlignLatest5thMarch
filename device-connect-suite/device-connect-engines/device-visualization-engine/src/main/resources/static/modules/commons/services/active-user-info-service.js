/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Stores current user which are sharable to others.
 */
define(['angular'], function (angular) {
    return angular.module('ui.app.modules.commons.service.active.user.info', [])
        .factory('ActiveUserInfoService', ['HttpService', '$rootScope', '$timeout', function (HttpService, $rootScope, $timeout) {

            var activeUser = {fName: '', lName: ''};
            
            var factory = {};
			
            /**
             * Returns active user.
             *
             * @returns {*}
             */
            factory.getActiveUserName = function () {
                return activeUser.logId;
            };
            
            /**
             * Sets the active user context.
             *
             * @param context
             */
            factory.setActiveUserName = function (context) {
                activeUser.logId = context;
            };
            
            /**
             * Returns active user First Name.
             *
             * @returns {*}
             */
            factory.getActiveUserFname = function () {
                return activeUser.fName;
            };
            
            /**
             * Sets the active user First Name.
             *
             * @param context
             */
            factory.setActiveUserFname = function (context) {
                activeUser.fName = context;
            };

            /**
             * Returns active user Last Name.
             *
             * @returns {*}
             */
            factory.getActiveUserLname = function () {
                return activeUser.lName;
            };
            

            /**
             * Returns active user phone.
             *
             * @returns {*}
             */
            factory.getActiveUserPhone = function () {
                return activeUser.phone;
            };
            

            /**
             * Returns active user email.
             *
             * @returns {*}
             */
            factory.getActiveUserEmail = function () {
                return activeUser.email;
            };
            
            /**
             * Sets the active user info context.
             *
             * @param context
             */
            factory.setActiveUserInfo = function (context) {
            	activeUser.logId = context.logId;
                activeUser.fName = context.fName;
                activeUser.lName = context.lName;
                activeUser.phone = context.phone;
                activeUser.email = context.email;
                activeUser.bedModify = context.bedModificationAllowed;
                activeUser.patientModify = context.patientModificationAllowed;
                
                $timeout(function () {
					$rootScope.$broadcast('load_user_view');
				}, 500);
            };
            
            factory.getActiveUserInfo = function () {
            	return activeUser;
            };
            
            
            factory.loadCurrentUser = function (userName) {
				
				HttpService.getByParms('get-profile-user', { params: { profileId: userName } },
					function (response) {
					
					factory.setActiveUserInfo(response.data);
						
					},
					function (error) {

					}
				);
            };
            


            /**
             * Returns active user roles.
             *
             * @returns {*}
             */
            factory.getUserRoles = function () {
                return activeUser.roles;
            };
            
            factory.setUserRoles = function (userRoles) {
				activeUser.roles = userRoles;
			};
			
			/**
             * Returns active user bed modification.
             *
             * @returns {*}
             */
			factory.getActiveUserBedModify = function () {
                return activeUser.bedModify;
            };
			
			/**
             * Returns active user patient modification.
             *
             * @returns {*}
             */
            factory.getActiveUserPatientModify = function () {
                return activeUser.patientModify;
            };
			
			return factory;
			
        }]);
});