/**
 * Copyright (c) 2017 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * The HTTP Service.
 *
 * An Angular Service wrapper atc as the HTTP Service facilitator
 * which exposes all HTTP methods such as GET, POST, PUT and DELETE.
 */
define(['angular', 'toastr'], function(angular, toastr){
    'use strict';
    return angular.module('ui.app.modules.commons.service.toastr.service', []).provider('ToastrService', [function(){
    	
        this.$get = function(){
        	
        	var initOptions = function() {
	    		toastr.options = {
	    			preventDuplicates: true,
	    			preventOpenDuplicates: true,
	    			progressBar: false,
	    			onclick: null
	        	}
	    	}
        	
            return{
            	
            	success: function(message, header) {
            		initOptions();
            		toastr.success(message, header);
            	},
            
	            warning: function(message, header) {
	            	initOptions();
	        		toastr.warning(message, header);
	        	},
            	
            	error: function(message, header) {
            		initOptions();
            		toastr.error(message, header);
            	},
	        	
	        	info: function(message, header) {
	        		initOptions();
            		toastr.info(message, header);
            	},
            	
            	warningByParam: function(message, header, clickEvent) {
            		initOptions();
            		
            		toastr.options.progressBar = true;
            		toastr.options.closeButton = true;
            		toastr.options.timeOut = 8000;
            		toastr.options.extendedTimeOut = 8000;
            		toastr.options.onclick = clickEvent;
            		
            		toastr.warning(message, header);
            	},
            	
            	alertMessage: function(message, header) {
            		initOptions();
            		
            		toastr.options.closeButton = true;
            		toastr.options.positionClass = 'toast-bottom-left';
            		
            		toastr.error(message, header);
            	}
            };
        };
    }]);
});