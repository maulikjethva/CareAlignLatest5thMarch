/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * The HTTP Service.
 *
 * An Angular Service wrapper atc as the HTTP Service facilitator
 * which exposes all HTTP methods such as GET, POST, PUT and DELETE.
 */
define(['angular'], function(angular){
    'use strict';
    return angular.module('ui.app.modules.commons.service.rest', []).provider('HttpService', [function(){

        this.baseURL = '';
        this.$get = function($http){

            var baseURL = this.baseURL;
            return{

                /**
                 *
                 * @param endpoint
                 * @param success
                 * @param failure
                 */
                get: function(endpoint, success, failure){
                    $http.get(baseURL + endpoint).then(success, failure);
                },

                /**
                 *
                 * @param endpoint
                 * @param parms
                 * @param success
                 * @param failure
                 */
                getByParms: function(endpoint, parms, success, failure){
                    $http.get(baseURL + endpoint, parms).then(success, failure);
                },

                /**
                 *
                 * @param endpoint
                 * @param data
                 * @param success
                 * @param failure
                 */
                create: function(endpoint, data, success, failure){
                    $http.post(baseURL + endpoint, JSON.stringify(data)).then(success, failure);
                },

                /**
                 *
                 * @param url
                 * @param endpoint
                 * @param data
                 * @param success
                 * @param failure
                 */
                createRemote: function(remoteURL, endpoint, data, success, failure){
                    $http.post(remoteURL + endpoint, JSON.stringify(data)).then(success, failure);
                },

                /**
                 *
                 * @param endpoint
                 * @param data
                 * @param success
                 * @param failure
                 */
                update: function(endpoint, data, success, failure){
                    $http.put(baseURL + endpoint, JSON.stringify(data)).then(success, failure);
                },

                /**
                 *
                 * @param endpoint
                 * @param success
                 * @param failure
                 */
                delete: function(endpoint, success, failure){
                    $http.delete(baseURL + endpoint).then(success, failure);
                },

                syncGet: function(endpoint, success, failure){

                    $q.ajax({
                        type: 'GET',
                        url: baseURL + endpoint,
                        async: false,
                        success: function(response){
                            success(response);
                        },
                        error: function(e){
                            failure(e);
                        }
                    });
                }
            };
        };
        this.setBaseURL = function(baseURL){
            this.baseURL = baseURL;
        };
    }]);
});