/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

define(["angular", "underscore"], function (angular, _) {
	"use strict";
	return angular.module("ui.app.modules.emr.controller", [])
	.controller("EMRController", ["$scope", "HttpService", function ($scope, HttpService) {
		
		$scope.selectedPatientDetails = [];
		$scope.patient = {};
		$scope.errInfo = false;
		
		$scope.searchClick = function(name) {
			$scope.selectedPatientDetails = [];
			$scope.patient = {};
			HttpService.getByParms("get-patient-info-by-name", { params: { patientName: name } },
				function (response) {
				
					var flag = response.data.length > 1;
					
					_.each(response.data, function(x){
						var displayInfo = { patientName: x.patientName, dentist: x.dentist, emailId: x.emailId, contact: x.contact, startTime: x.startTime, endTime: x.endTime, patientAge: x.patientAge, patientGender: x.patientGender, patientPlace: x.patientPlace }
						if(flag) {
							$scope.selectedPatientDetails.push(displayInfo);
						}
						else {
							$scope.patient = displayInfo;
						}
					});
					
					if(!response.data.length || response.data.patientName){
						$scope.errInfo = true;
					}
					else {
						$scope.errInfo = false;
					}
					$scope.surfaceSave();
				},
				function (error) {
	
				}
			);
		}
		
		$scope.selectedPatient = function(patient) {
			$scope.patient = patient;
		}
		
//		$scope.searchClick = function(name) {
//			console.log(name);
//			var data = { name: "Nishanth", description: "qwerty" };
//			HttpService.create("save-patient-info-by-name", data,
//				function (response) {
//					console.log(response.data);
//				},
//				function (error) {
//					console.log(error);
//				}
//			);
//		}
		
		
		/*
		 * Consultation Controller
		 */
		$scope.imgEvent = function(e) {
			var offset = $(e.target).offset();
			
			var x = ((e.pageX - offset.left) / $(e.target).outerWidth() * 100);
			var y = ((e.pageY - offset.top) / $(e.target).outerHeight() * 100);
			
//			console.log(x+"\n"+y);
			
			/*
			 * Condition is to select teeth number based on the image
			 */
			
			$scope.selectedSurface = $scope.selectedStatus = $scope.selectedSpecialty = $scope.selectedCategory = $scope.selectedTreatment = "--select one option--";
			
//			angular.element('#plannedDate').value = angular.element('#performedDate').value = '';
			
			if($scope.patient.patientAge >= 14) {
				if((x >= "8.333333333333332" && x <= "13.596491228070176") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "18";
				}
				else if((x >= "14.320388349514563" && x <= "19.902912621359224") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "17";
				}
				else if((x >= "20.631067961165048" && x <= "26.45631067961165") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "16";
				}
				else if((x >= "27.184466019417474" && x <= "30.582524271844658") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "15";
				}
				else if((x >= "31.31067961165049" && x <= "35.19417475728155") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "14";
				}
				else if((x >= "35.67961165048544" && x <= "40.29126213592233") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "13";
				}
				else if((x >= "40.77669902912621" && x <= "44.1747572815534") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "12";
				}
				else if((x >= "44.90291262135923" && x <= "49.271844660194176") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "11";
				}
				else if((x >= "50" && x <= "54.36893203883495") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "21";
				}
				else if((x >= "55.09708737864077" && x <= "58.495145631067956") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "22";
				}
				else if((x >= "59.22330097087378" && x <= "63.3495145631068") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "23";
				}
				else if((x >= "64.07766990291263" && x <= "67.96116504854369") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "24";
				}
				else if((x >= "68.44660194174757" && x <= "71.84466019417476") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "25";
				}
				else if((x >= "72.57281553398059" && x <= "78.64077669902912") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "26";
				}
				else if((x >= "79.12621359223301" && x <= "84.9514563106796") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "27";
				}
				else if((x >= "85.43689320388349" && x <= "91.01941747572816") && (y >= "33.860759493670884" && y <= "50.31645569620253")) {
					$scope.consultation.teethNo = "28";
				}
				
				else if((x >= "9.951456310679612" && x <= "15.291262135922329") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "48";
				}
				else if((x >= "16.019417475728158" && x <= "21.35922330097087") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "47";
				}
				else if((x >= "22.330097087378643" && x <= "27.9126213592233") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "46";
				}
				else if((x >= "28.398058252427184" && x <= "32.76699029126214") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "45";
				}
				else if((x >= "33.00970873786408" && x <= "37.62135922330097") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "44";
				}
				else if((x >= "38.349514563106794" && x <= "42.23300970873786") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "43";
				}
				else if((x >= "43.203883495145625" && x <= "45.87378640776699") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "42";
				}
				else if((x >= "46.601941747572816" && x <= "49.51456310679612") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "41";
				}
				else if((x >= "50.24271844660194" && x <= "52.9126213592233") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "31";
				}
				else if((x >= "53.640776699029125" && x <= "56.310679611650485") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "32";
				}
				else if((x >= "57.03883495145631" && x <= "61.165048543689316") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "33";
				}
				else if((x >= "61.89320388349514" && x <= "66.2621359223301") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "34";
				}
				else if((x >= "66.74757281553399" && x <= "70.87378640776699") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "35";
				}
				else if((x >= "71.35922330097088" && x <= "77.18446601941747") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "36";
				}
				else if((x >= "78.15533980582524" && x <= "83.49514563106796") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "37";
				}
				else if((x >= "83.98058252427184" && x <= "89.56310679611651") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "38";
				}
				
				else {
					$scope.consultation.teethNo = "";
				}
			}
			else {
				if((x >= "14.322250639386189" && x <= "22.25063938618926") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "55";
				}
				else if((x >= "23.52941176470588" && x <= "30.434782608695656") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "54";
				}
				else if((x >= "31.202046035805626" && x <= "37.59590792838875") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "53";
				}
				else if((x >= "37.59590792838875" && x <= "43.22250639386189") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "52";
				}
				else if((x >= "43.47826086956522" && x <= "48.59335038363171") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "51";
				}
				else if((x >= "49.87212276214834" && x <= "55.24296675191815") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "61";
				}
				else if((x >= "55.24296675191815" && x <= "60.86956521739131") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "62";
				}
				else if((x >= "60.86956521739132" && x <= "67.26342710997443") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "63";
				}
				else if((x >= "68.03069053708441" && x <= "74.93606138107417") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "64";
				}
				else if((x >= "76.47058823529412" && x <= "84.14322250639387") && (y >= "27.340823970037455" && y <= "52.80898876404494")) {
					$scope.consultation.teethNo = "65";
				}
				
				else if((x >= "15.34526854219949" && x <= "23.52941176470588") && (y >= "54.68164794007491" && y <= "77.15355805243446")) {
					$scope.consultation.teethNo = "85";
				}
				else if((x >= "24.55242966751918" && x <= "31.9693094629156") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "84";
				}
				else if((x >= "33.248081841432224" && x <= "37.851662404092075") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "83";
				}
				else if((x >= "40.40920716112532" && x <= "44.24552429667519") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "82";
				}
				else if((x >= "45.52429667519181" && x <= "48.59335038363171") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "81";
				}
				else if((x >= "49.87212276214834" && x <= "53.96419437340153") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "71";
				}
				else if((x >= "54.47570332480819" && x <= "58.82352941176471") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "72";
				}
				else if((x >= "60.61381074168798" && x <= "65.21739130434783") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "73";
				}
				else if((x >= "66.49616368286445" && x <= "73.91304347826086") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "74";
				}
				else if((x >= "75.1918158567775" && x <= "83.37595907928389") && (y >= "51.578947368421055" && y <= "68.42105263157895")) {
					$scope.consultation.teethNo = "75";
				}
				
				else {
					$scope.consultation.teethNo = "";
				}
			}
		}
		
		
		/*
		 * temporary
		 */
//		$scope.surfaceSave = function() {
//			HttpService.create("save-specialty-info", $scope.specialty, function (response) {
//					console.log('here')
//					console.log(response.data);
//				},
//				function (error) {
//	
//				}
//			);
//		}
		$scope.surfaceSave = function() {
			HttpService.get("get-surface-info", function (response) {
					$scope.surface = response.data;
					$scope.statusSave();
				},
				function (error) {
	
				}
			);
		}
		$scope.statusSave = function() {
			HttpService.get("get-status-info", function (response) {
					$scope.status = response.data;
					$scope.specialtySave();
				},
				function (error) {
	
				}
			);
		}
		$scope.specialtySave = function() {
			HttpService.get("get-specialty-info", function (response) {
					$scope.specialty= response.data;
				},
				function (error) {
	
				}
			);
		}
		$scope.categorySave = function(value) {
			$scope.selectedCategory = "--select one option--";
			$scope.category = [];
			$scope.selectedTreatment = "--select one option--";
			$scope.treatment = [];
			HttpService.getByParms("get-category-info", { params: { data : value } }, function (response) {
					$scope.category= response.data;
				},
				function (error) {
	
				}
			);
		}
		$scope.treatmentSave = function(value) {
			$scope.selectedTreatment = "--select one option--";
			$scope.treatment = [];
			HttpService.getByParms("get-treatment-info", { params: { data : value } }, function (response) {
					$scope.treatment = response.data;
				},
				function (error) {
	
				}
			);
		}
		
		
		/*$scope.surface = [{value: "Buccal"}, {value: "Distal"}, {value: "Mesial"}, {value: "Occlusal"}, {value: "Palatal/Lingual"}];
		$scope.status = [{value: "Planned"}, {value: "Advised"}, {value: "Inprogress"}, {value: "Performed"}];
		$scope.specialty = [{value: "Cosmetic Dentistry"}, 
								{value: "Endodontist"}, 
								{value: "Oral Medicine & Radiology"}, 
								{value: "Oral Surgery"}, 
								{value: "Orthodontist"}, 
								{value: "Pedodontist"}, 
								{value: "Periodontics"}, 
								{value: "Pharmacy"}, 
								{value: "Prosthodontist"}, 
								{value: "Restorative Dentistry"}];*/
		
		/*$scope.category = [{value: "Bleaching", ref: "Cosmetic Dentistry"}, 
							{value: "Diastema Closure", ref: "Cosmetic Dentistry"}, 
							{value: "Teeth Jewel", ref: "Cosmetic Dentistry"},
							
							{value: "Apexification (Excluding Rct Charges)", ref: "Endodontist"}, 
							{value: "Deep Carries Management", ref: "Endodontist"}, 
							{value: "Post Endodontic Restoration", ref: "Endodontist"}, 
							{value: "Pulp Capping", ref: "Endodontist"}, 
							{value: "Pulpotomy (Therapeutic Pulpotomy)", ref: "Endodontist"}, 
							{value: "Re Root Canal Treatment (Re- Rct)", ref: "Endodontist"}, 
							{value: "Root Canal Treatment(Rct)", ref: "Endodontist"},
							
							{value: "Confident Smile Program", ref: "Oral Medicine & Radiology"}, 
							{value: "Extra Oral Radiology", ref: "Oral Medicine & Radiology"}, 
							{value: "Intar Oral Radiology", ref: "Oral Medicine & Radiology"}, 
							
							{value: "Dental Implants", ref: "Medicine & Radiology"}, 
							{value: "Endodontics (Root Canal Treatment, Rct)", ref: "Medicine & Radiology"}, 
							{value: "Extractions", ref: "Medicine & Radiology"},
							{value: "Impactions", ref: "Medicine & Radiology"}, 
							{value: "Oral Procedure", ref: "Medicine & Radiology"}, 
							{value: "Other Surgeries", ref: "Medicine & Radiology"}, 
							{value: "Radiology Lab", ref: "Medicine & Radiology"}, 
							
							{value: "Fixed Ortho Appliances", ref: "Oral Surgery"}, 
							{value: "Microscrew Implant (Each)", ref: "Oral Surgery"}, 
							{value: "Removal Ortho Appliances", ref: "Oral Surgery"}, 
							
							{value: "Paediatric Endodontics", ref: "Orthodontist"}, 
							{value: "Paediatric Oral Surgery", ref: "Orthodontist"}, 
							{value: "Paediatric Orthodontics", ref: "Orthodontist"}, 
							{value: "Preventive Paediatric Dentistry", ref: "Orthodontist"}, 
							{value: "Restorative  Dentistry", ref: "Orthodontist"}, 
							{value: "Restorative Paediatric Dentistry", ref: "Orthodontist"}, 
							
							{value: "Dental Prophylaxis", ref: "Pedodontist"}, 
							{value: "Flap Surgeries With Laser", ref: "Pedodontist"}, 
							{value: "Mucogingival Surgeries", ref: "Pedodontist"}, 
							{value: "Non-Surgical Perodontal Services", ref: "Pedodontist"}, 
							{value: "Surgical Perodontal Services", ref: "Pedodontist"}, 
							
							{value: "Bone Graft Material", ref: "Periodontics"}, 
							{value: "Conscious Sedation Per Hour", ref: "Periodontics"}, 
							{value: "Membrane", ref: "Periodontics"}, 
							
							{value: "Complete Denture (Cd)", ref: "Pharmacy"}, 
							{value: "Crowns", ref: "Pharmacy"}, 
							{value: "Denture Relining", ref: "Pharmacy"}, 
							{value: "Denture Repair", ref: "Pharmacy"}, 
							{value: "Occlusion", ref: "Pharmacy"}, 
							{value: "Partial Denture (Pd)", ref: "Pharmacy"}, 
							{value: "Smile Designing", ref: "Pharmacy"}, 
							
							{value: "Composite Filling", ref: "Restorative Dentistry"}, 
							{value: "Glass Ionomer Filling", ref: "Restorative Dentistry"}, 
							{value: "Inlays", ref: "Restorative Dentistry"}, 
							{value: "Onlays", ref: "Restorative Dentistry"}, 
							{value: "Temporary Filling", ref: "Restorative Dentistry"}];
		
		$scope.treatment = [{value: "Bleaching(Home)", ref: "Bleaching"},
							{value: "Bleaching(in office)", ref: "Bleaching"},
							{value: "Home bleach/office bleach", ref: "Bleaching"},
							{value: "Home bleaching", ref: "Bleaching"},
							{value: "In office bleaching", ref: "Bleaching"},
							{value: "walk in bleaching", ref: "Bleaching"},
							
							{value: "Diastema Closure", ref: "Diastema Closure"},
							
							{value: "Teeth Jewel(Each tooth)", ref: "Teeth Jewel"},
							
							{value: "Apexification (MTA) Single visit", ref: "Apexification (Excluding RCT Charges)"},
							{value: "Preforation Repair (MTA)", ref: "Apexification (Excluding RCT Charges)"},
							{value: "Regular Apexification Multiple visits)", ref: "Apexification (Excluding RCT Charges)"},
							
							{value: "Direct Pulp Capping", ref: "Deep Carries Management"},
							{value: "Direct Pulp Capping-Laser", ref: "Deep Carries Management"},
							{value: "Indirect Pulp Capping", ref: "Deep Carries Management"},
							{value: "Indirect Pulp Capping-Laser", ref: "Deep Carries Management"},
							
							{value: "Core Build up", ref: "Post Endodontic Restoration"},
							{value: "Custom Made (Cast Post)", ref: "Post Endodontic Restoration"},
							{value: "Pre-fabricated (fiber post or Lc post)", ref: "Post Endodontic Restoration"},
							{value: "Pre-fabricated (Metal pots)", ref: "Post Endodontic Restoration"},
							
							{value: "Pulp capping", ref: "Pulp Capping"},
							
							{value: "Pulpotomy (therapeutic Pulpotomy)", ref: "Pulpotomy (therapeutic Pulpotomy)"},
							
							{value: "Re-RCT -Anterior -Laser and Microscope", ref: "Re Root Canal Treatment (Re- RCT)"},
							{value: "Re-RCT -Posterior -Laser and Microscope", ref: "Re Root Canal Treatment (Re- RCT)"},
							
							{value: "RCT -Anterior", ref: "Root Canal Treatment(RCT)"},
							{value: "RCT -Anterior- 1", ref: "Root Canal Treatment(RCT)"},
							{value: "RCT -Anterior-Laser", ref: "Root Canal Treatment(RCT)"},
							{value: "RCT -Anterior-Microscope", ref: "Root Canal Treatment(RCT)"},
							{value: "RCT -Posterior", ref: "Root Canal Treatment(RCT)"},
							{value: "RCT -Posterior- 1", ref: "Root Canal Treatment(RCT)"},
							{value: "RCT -Posterior-Laser", ref: "Root Canal Treatment(RCT)"},
							{value: "RCT -Posterior-Microscope", ref: "Root Canal Treatment(RCT)"},
							
							{value: "1st class composit (tooth coloured) restoration", ref: "Confident Smile Program"},
							{value: "Consultation", ref: "Confident Smile Program"},
							{value: "Fit and fissure sealant application to prevent carries", ref: "Confident Smile Program"},
							{value: "Scaling and Polishing", ref: "Confident Smile Program"},
							{value: "Smile Program", ref: "Confident Smile Program"},
							{value: "Stabilization of Dental  emergencies", ref: "Confident Smile Program"},
							{value: "Unlimited IOPA x-rays", ref: "Confident Smile Program"},
							
							{value: "CBCT-Full Mouth", ref: "Extra Oral Radiology"},
							{value: "CBCT-Per Arch", ref: "Extra Oral Radiology"},
							{value: "CBCT-Per Quadrant", ref: "Extra Oral Radiology"},
							{value: "Cethalogram", ref: "Extra Oral Radiology"},
							{value: "Cethalogram prediction analysis", ref: "Extra Oral Radiology"},
							{value: "Cethalogram with computer analysis", ref: "Extra Oral Radiology"},
							{value: "Hand Wrist X-ray", ref: "Extra Oral Radiology"},
							{value: "OPD Digital CD", ref: "Extra Oral Radiology"},
							{value: "OPD-Digital (Orthopantomography)", ref: "Extra Oral Radiology"},
							{value: "TMJ-X-Ray both side (open & closed)", ref: "Extra Oral Radiology"},
							{value: "TMJ-X-Ray one side (open & closed)", ref: "Extra Oral Radiology"},
							
							{value: "Bitewing", ref: "Intar Oral Radiology"},
							{value: "IOPA", ref: "Intar Oral Radiology"},
							{value: "Occusal Radiography", ref: "Intar Oral Radiology"},
							{value: "RVG", ref: "Intar Oral Radiology"},
							
							{value: "All on Four", ref: "Dental Implants"},
							{value: "CAD-CAM Miled Bar", ref: "Dental Implants"},
							{value: "Computer Guided  Impleant Placement(1-2 teeth)", ref: "Dental Implants"},
							{value: "Computer Guided  Impleant Placement(3-4 teeth)", ref: "Dental Implants"},
							{value: "Computer Guided Imp placement Stent(Full mouth)", ref: "Dental Implants"},
							{value: "Customized Abutments in titanium", ref: "Dental Implants"},
							{value: "Customized Abuttement / unit", ref: "Dental Implants"},
							{value: "Customized Zirconia Abutments CAD / CAM", ref: "Dental Implants"},
							{value: "Guided Bone regeneration (Ridge Augmentation)", ref: "Dental Implants"},
							{value: "Immediate loading / tooth ", ref: "Dental Implants"},
							{value: "Immediate Temporization / tooth", ref: "Dental Implants"},
							{value: "Implant Including Crown", ref: "Dental Implants"},
							{value: "Implant with Crowns2", ref: "Dental Implants"},
							{value: "Nobel Biocare Implant with Crown", ref: "Dental Implants"},
							{value: "Socket Preservation", ref: "Dental Implants"},
							{value: "Titanium Mesh", ref: "Dental Implants"},
							{value: "Titanium Mesh Vertical Augmentation without material", ref: "Dental Implants"},
							{value: "Zirconia Abutments", ref: "Dental Implants"},
							
							{value: "RCT Re-Dressing with Metapex", ref: "Endodontics (root canal treatment, RCT)"},
							{value: "RCT Re-Dressing with MTA", ref: "Endodontics (root canal treatment, RCT)"},
							{value: "Walk  bleaching", ref: "Endodontics (root canal treatment, RCT)"},
							
							{value: "Extraction", ref: "Extractions"},
							{value: "Extraction Child", ref: "Extractions"},
							{value: "Extraction Complex", ref: "Extractions"},
							{value: "Open Method Extraction / Surgical Extraction", ref: "Extractions"},
							
							{value: "Impaction under L.A Complex", ref: "Impactions"},
							{value: "Impaction under L.A Simple", ref: "Impactions"},
							
							{value: "Alcohol injection (neural)", ref: "Oral Procedure"},
							{value: "Alveoloplasty per qudrant", ref: "Oral Procedure"},
							{value: "AMO (Anterior Maxillary Ostetomy)", ref: "Oral Procedure"},
							{value: "Apicoectomy (ANTERIOR Tooth)", ref: "Oral Procedure"},
							{value: "Apicoectomy (Multiple ANTERIOR Teeth)", ref: "Oral Procedure"},
							{value: "Apicoectomy (Multiple Posterior Teeth)", ref: "Oral Procedure"},
							{value: "Apicoectomy (Premolar / Molar)", ref: "Oral Procedure"},
							{value: "Botox injection", ref: "Oral Procedure"},
							{value: "Cyst Enucleation(Large)", ref: "Oral Procedure"},
							{value: "Cyst Enucleation(SMALL)", ref: "Oral Procedure"},
							{value: "Dento Alveolar Fracture Splinting", ref: "Oral Procedure"},
							{value: "Dry Socket dressing", ref: "Oral Procedure"},
							{value: "Enameloplasty", ref: "Oral Procedure"},
							{value: "FracturArch Bar Fixation", ref: "Oral Procedure"},
							{value: "Fracture plating", ref: "Oral Procedure"},
							{value: "Frenectomy (Labial)", ref: "Oral Procedure"},
							{value: "Frenectomy (Lingual)", ref: "Oral Procedure"},
							{value: "Genioplasty", ref: "Oral Procedure"},
							{value: "Hemisection (Excluding RCT)", ref: "Oral Procedure"},
							{value: "Incision and Drainage (MAJOR)", ref: "Oral Procedure"},
							{value: "Incision and Drainage (MINOR)", ref: "Oral Procedure"},
							{value: "Inter-Maxillary Fixation (IMF) and  eyelet wiring", ref: "Oral Procedure"},
							{value: "Jaw Dislocation Reduction", ref: "Oral Procedure"},
							{value: "Mucocele Excision", ref: "Oral Procedure"},
							{value: "Operculectomy", ref: "Oral Procedure"},
							{value: "Oro-Antral fistula closure", ref: "Oral Procedure"},
							{value: "Osteotomy - Double Jaw", ref: "Oral Procedure"},
							{value: "Osteotomy - single Jaw", ref: "Oral Procedure"},
							{value: "Peripheral Neurectomy", ref: "Oral Procedure"},
							{value: "Root resection", ref: "Oral Procedure"},
							{value: "Saturing", ref: "Oral Procedure"},
							{value: "Sinus Lift", ref: "Oral Procedure"},
							{value: "Soft tissue growth removal", ref: "Oral Procedure"},
							{value: "Sub-Mucosal Injection (Each Visit)", ref: "Oral Procedure"},
							{value: "TMJ Lavage", ref: "Oral Procedure"},
							{value: "TMJ Steroid Injection", ref: "Oral Procedure"},
							{value: "Tooth Reimplant", ref: "Oral Procedure"},
							
							{value: "Abscess Incision And Drainage", ref: "Other Surgeries"},
							{value: "Alveoloplasty (each quadrant)", ref: "Other Surgeries"},
							{value: "Apisectomy", ref: "Other Surgeries"},
							{value: "Arch Bar wiring and IMF", ref: "Other Surgeries"},
							{value: "Biopsy", ref: "Other Surgeries"},
							{value: "Block Graft from chin/Ramus/Tuberosity", ref: "Other Surgeries"},
							{value: "Cyst Enucleation", ref: "Other Surgeries"},
							{value: "Fracture Reduction and Fixation - Open Method", ref: "Other Surgeries"},
							{value: "Intra-Lesion Injection", ref: "Other Surgeries"},
							{value: "Major Oral Surgeries under L.A.", ref: "Other Surgeries"},
							{value: "Major Oral Surgeries under L.A. with Conscious Sed", ref: "Other Surgeries"},
							{value: "Oral Surgery under General Anaesthesia", ref: "Other Surgeries"},
							{value: "Osteotomy", ref: "Other Surgeries"},
							{value: "Removable Plate Surgical", ref: "Other Surgeries"},
							{value: "Sinus Lift Procedure", ref: "Other Surgeries"},
							{value: "Splinting", ref: "Other Surgeries"},
							{value: "Surgical Excision", ref: "Other Surgeries"},
							{value: "Surgical Excision-Large Lesion(Adv / Complicated)", ref: "Other Surgeries"},
							{value: "TMJ Steroid Injection", ref: "Other Surgeries"},
							
							{value: "CBCT Scan - 1 Quadrant", ref: "Radiology Lab"},
							{value: "CBCT Scan Upper / Lower Arch", ref: "Radiology Lab"},
							
							{value: "Fixed Functional Appliance", ref: "Fixed Ortho Appliances"},
							{value: "Fixed Retainers - lower", ref: "Fixed Ortho Appliances"},
							{value: "Fixed Retainers - upper", ref: "Fixed Ortho Appliances"},
							{value: "Invisalign", ref: "Fixed Ortho Appliances"},
							{value: "Lingual Ortodontic appliance", ref: "Fixed Ortho Appliances"},
							{value: "Self Ligating Bracket  Metal", ref: "Fixed Ortho Appliances"},
							{value: "Self ligating bracket ceramic", ref: "Fixed Ortho Appliances"},
							{value: "Straight wire appliance -  Ceramic", ref: "Fixed Ortho Appliances"},
							{value: "Straight wire appliance -  Ceramic(Single Arch)", ref: "Fixed Ortho Appliances"},
							{value: "Straight wire appliance - Metal", ref: "Fixed Ortho Appliances"},
							{value: "Straight wire appliance - Metal(Single Arch)", ref: "Fixed Ortho Appliances"},
							
							{value: "Microscrew implant (Each)", ref: "Microscrew Implant (Each)"},
							
							{value: "Activator", ref: "Removal Ortho Appliances"},
							{value: "Bionator", ref: "Removal Ortho Appliances"},
							{value: "Chin Cap", ref: "Removal Ortho Appliances"},
							{value: "Face Mask", ref: "Removal Ortho Appliances"},
							{value: "Frankel  III/ II/I", ref: "Removal Ortho Appliances"},
							{value: "Headgear (High Pull, Low Pull)", ref: "Removal Ortho Appliances"},
							{value: "Jack screw", ref: "Removal Ortho Appliances"},
							{value: "Lip Bumper (Regular)", ref: "Removal Ortho Appliances"},
							{value: "Lower Hawley's Appliance (UHA)", ref: "Removal Ortho Appliances"},
							{value: "Quad helix", ref: "Removal Ortho Appliances"},
							{value: "Rapid Palatal Expansion Appliance (RPE)", ref: "Removal Ortho Appliances"},
							{value: "Reverse Pull Headgear", ref: "Removal Ortho Appliances"},
							{value: "Soft Splint-one arch", ref: "Removal Ortho Appliances"},
							{value: "Tongue spike", ref: "Removal Ortho Appliances"},
							{value: "Twin Block", ref: "Removal Ortho Appliances"},
							{value: "Upper Hawley's Appliance (UHA)", ref: "Removal Ortho Appliances"},
							
							{value: "Pulpectomy", ref: "Paediatric Endodontics"},
							{value: "Pulpotomy", ref: "Paediatric Endodontics"},
							
							{value: "Extraction - Pedo", ref: "Paediatric Oral Surgery"},
							{value: "Frenectomy", ref: "Paediatric Oral Surgery"},
							{value: "Frenectomy with laser", ref: "Paediatric Oral Surgery"},
							{value: "Surgical removal of Impacted teeth", ref: "Paediatric Oral Surgery"},
							{value: "Surgical removal of tooth", ref: "Paediatric Oral Surgery"},
							
							{value: "2/4 Appliance - Utility Arch", ref: "Paediatric Orthodontics"},
							{value: "Inclined plane", ref: "Paediatric Orthodontics"},
							{value: "Lip Bumper Pedo", ref: "Paediatric Orthodontics"},
							{value: "Oral Screen", ref: "Paediatric Orthodontics"},
							{value: "PEDO Removable Acrylic Partial Denture (1 Unit)", ref: "Paediatric Orthodontics"},
							{value: "PEDO Removable Acrylic Partial Denture (Every Additional Unit)", ref: "Paediatric Orthodontics"},
							{value: "Proximal Stripping (per tooth)(Anterior/Posterior)", ref: "Paediatric Orthodontics"},
							{value: "Soft splint for Bruxism management", ref: "Paediatric Orthodontics"},
							{value: "Space Maintainer (band and loop)", ref: "Paediatric Orthodontics"},
							{value: "Space Maintainer (lingual arch)", ref: "Paediatric Orthodontics"},
							{value: "Space Maintainer (nance- upper arch)", ref: "Paediatric Orthodontics"},
							{value: "Space regainer", ref: "Paediatric Orthodontics"},
							{value: "Transpalatal arch space maintainer", ref: "Paediatric Orthodontics"},
							{value: "Upper Hawley's appliance", ref: "Paediatric Orthodontics"},
							{value: "Upper Hawley's appliance with spring", ref: "Paediatric Orthodontics"},
							{value: "Upper Hawley's appliance with spring and expansion screw", ref: "Paediatric Orthodontics"},
							{value: "Upper Hawley's with finger spikes", ref: "Paediatric Orthodontics"},
							
							{value: "Fluoride gel (tray) per arch", ref: "Preventive Paediatric Dentistry"},
							{value: "Fluoride varnish (per quadrant)", ref: "Preventive Paediatric Dentistry"},
							{value: "Fluoride varnish (per tooth)", ref: "Preventive Paediatric Dentistry"},
							{value: "Pit & Fissure Sealant (Per tooth)", ref: "Preventive Paediatric Dentistry"},
							{value: "Scaling (Oral prophylaxis", ref: "Preventive Paediatric Dentistry"},
							
							{value: "Composit", ref: "Restorative Paediatric Dentistry"},
							{value: "GIC", ref: "Restorative Paediatric Dentistry"},
							{value: "IRM filling", ref: "Restorative Paediatric Dentistry"},
							{value: "PEDO Indirect pulpcaping", ref: "Restorative Paediatric Dentistry"},
							{value: "Stainless Steel Crown", ref: "Restorative Paediatric Dentistry"},
							{value: "Strip crown (one)", ref: "Restorative Paediatric Dentistry"},
							
							{value: "Deep Scaling (Under LA) full mouth", ref: "Dental Prophylaxis"},
							{value: "Gross Scaling per sitting", ref: "Dental Prophylaxis"},
							{value: "Laser Bleaching (in office)", ref: "Dental Prophylaxis"},
							{value: "Polishing", ref: "Dental Prophylaxis"},
							{value: "Scaling", ref: "Dental Prophylaxis"},
							{value: "Scaling and Polishing", ref: "Dental Prophylaxis"},
							
							{value: "Flap surgery (1-4 teeth)- LASER", ref: "Flap Surgeries With Laser"},
							{value: "Flap surgery -full mouth - LASER", ref: "Flap Surgeries With Laser"},
							{value: "Flap surgery per quadrant- LASER", ref: "Flap Surgeries With Laser"},
							{value: "Flap surgery with root biomodification- LASER", ref: "Flap Surgeries With Laser"},
							
							
							{value: "Apically repositioned flap", ref: "Mucogingival Surgeries"},
							{value: "Coronally advanced flap", ref: "Mucogingival Surgeries"},
							{value: "Desensitization -LASER", ref: "Mucogingival Surgeries"},
							{value: "Free gingival graft", ref: "Mucogingival Surgeries"},
							{value: "GTR (1-4 teeth)", ref: "Mucogingival Surgeries"},
							{value: "Laterally repositioned flap", ref: "Mucogingival Surgeries"},
							{value: "Papilla reconstruction", ref: "Mucogingival Surgeries"},
							{value: "Root coverage procedures using synthetic grafts", ref: "Mucogingival Surgeries"},
							{value: "Soft tissue graft", ref: "Mucogingival Surgeries"},
							{value: "Vestibuloplasty", ref: "Mucogingival Surgeries"},
							
							{value: "Composit Splinting (1- 4 teeth)", ref: "Non-Surgical Perodontal Services"},
							{value: "Composit Splinting (more than  4 teeth)", ref: "Non-Surgical Perodontal Services"},
							{value: "Curettage (per quadrant)- LASER", ref: "Non-Surgical Perodontal Services"},
							{value: "Deep Scaling (full mouth)", ref: "Non-Surgical Perodontal Services"},
							{value: "Fiber Splinting (more than 6 teeth)", ref: "Non-Surgical Perodontal Services"},
							{value: "Fiber Splinting (up to 6 teeth)", ref: "Non-Surgical Perodontal Services"},
							{value: "Localized delivery of Antimicrobial agents (Per Tooth)", ref: "Non-Surgical Perodontal Services"},
							{value: "Root planning  5-8 teeth or per quadrant", ref: "Non-Surgical Perodontal Services"},
							{value: "Root planning (1-4 teeth)", ref: "Non-Surgical Perodontal Services"},
							{value: "Root planning with Laser Pocket dissinfection", ref: "Non-Surgical Perodontal Services"},
							
							{value: "Crown lengthening", ref: "Surgical Perodontal Services"},
							{value: "Crown lengthening with lasers", ref: "Surgical Perodontal Services"},
							{value: "Flap Surgery (1-4 teeth)", ref: "Surgical Perodontal Services"},
							{value: "Flap Surgery- full month", ref: "Surgical Perodontal Services"},
							{value: "Flap Surgery per quadrant", ref: "Surgical Perodontal Services"},
							{value: "Flap Surgery with root root modification", ref: "Surgical Perodontal Services"},
							{value: "Frenectomy- SURGICAL", ref: "Surgical Perodontal Services"},
							{value: "Frenectomy with laser", ref: "Surgical Perodontal Services"},
							{value: "Gingival depigmentation - (each arch) Laser", ref: "Surgical Perodontal Services"},
							{value: "Gingival depigmentation - (each arch) surgical", ref: "Surgical Perodontal Services"},
							{value: "Gingivectomy (1-4 teeth)", ref: "Surgical Perodontal Services"},
							{value: "Gingivectomy (per quadrant)", ref: "Surgical Perodontal Services"},
							{value: "Gingivectomy- Laser", ref: "Surgical Perodontal Services"},
							{value: "Gingivoplasty(1-4 teeth)", ref: "Surgical Perodontal Services"},
							{value: "Gingivoplasty(per quadrant)", ref: "Surgical Perodontal Services"},
							{value: "Operculectomy", ref: "Surgical Perodontal Services"},
							{value: "Pericision", ref: "Surgical Perodontal Services"},
							{value: "Ridge Augmentation/ each arch", ref: "Surgical Perodontal Services"},
							{value: "Socket Preservation (1-2 teeth)", ref: "Surgical Perodontal Services"},
							{value: "Surgical crown exposure", ref: "Surgical Perodontal Services"},
							
							{value: "Bone Graft Perioglass", ref: "Bone Graft Material"},
							
							{value: "Conscious Sedation For Adults", ref: "Conscious Sedation Per Hour"},
							{value: "Conscious Sedation For Childrens", ref: "Conscious Sedation Per Hour"},
							
							{value: "Membrane 15x20", ref: "Membrane"},
							{value: "Membrane 20x30", ref: "Membrane"},
							
							{value: "Bio Functional Prosthesis", ref: "Complete Denture (Cd)"},
							{value: "Completed Denture Metal", ref: "Complete Denture (Cd)"},
							{value: "Completed Denture U/L, 2 implants with ball attachment", ref: "Complete Denture (Cd)"},
							{value: "Immediate Dentures without extraction", ref: "Complete Denture (Cd)"},
							{value: "Single Denture (CD) with Acrylic Base", ref: "Complete Denture (Cd)"},
							{value: "Single Denture (CD) with Cast Metal Framework", ref: "Complete Denture (Cd)"},
							{value: "Single Denture (SD) (Lower)", ref: "Complete Denture (Cd)"},
							{value: "Single Denture (Upper)", ref: "Complete Denture (Cd)"},
							{value: "Single Denture with metallic base (Lower)", ref: "Complete Denture (Cd)"},
							{value: "Single Denture with metallic base (Upper)", ref: "Complete Denture (Cd)"},
							{value: "Two Implant Retained Acrylic  Over Denture", ref: "Complete Denture (Cd)"},
							{value: "Two Implant Supported Denture with Ball Attachments", ref: "Complete Denture (Cd)"},
							{value: "Two Implant Supported Denture with Ball Locator", ref: "Complete Denture (Cd)"},
							
							{value: "Acrylic Crown (JC) / Provisional Crowns/ per unit", ref: "Crowns"},
							{value: "CEREC(CAD/CAM) Crown/per unit", ref: "Crowns"},
							{value: "Full metal (Gold) crown/per unit", ref: "Crowns"},
							{value: "Full metal (NiCr) crown/per unit", ref: "Crowns"},
							{value: "Maryaland Bridge per Unit-Acrylic", ref: "Crowns"},
							{value: "Maryaland Bridge per Unit-Ceramic", ref: "Crowns"},
							{value: "Metal crown  with Ceramic facing", ref: "Crowns"},
							{value: "Metal crown (Ni Cr) with Acrylic facing", ref: "Crowns"},
							{value: "Metal Free Ceramic crown/per unit Premium", ref: "Crowns"},
							{value: "Metal Free Ceramic crown/per unit Regular", ref: "Crowns"},
							{value: "PFM", ref: "Crowns"},
							{value: "PFM Crown-1", ref: "Crowns"},
							{value: "Poly Corbonate crown", ref: "Crowns"},
							{value: "Recementation of crown", ref: "Crowns"},
							{value: "Zirconia crown per unit", ref: "Crowns"},
							{value: "Zirconia Crown-1", ref: "Crowns"},
							{value: "Zirconia premium crown per unit", ref: "Crowns"},
							
							{value: "Soft Relining(each denture)", ref: "Denture Relining"},
							
							{value: "Denture Repair", ref: "Denture Repair"},
							{value: "Denture Special Compression", ref: "Denture Repair"},
							
							
							{value: "Cast partial Denture (Cast PD) upto 2 Units/Upper", ref: "Partial Denture (Pd)"},
							{value: "Cast PD -Every Additional Pression Attachment/Upper", ref: "Partial Denture (Pd)"},
							{value: "Partial Denture ( Acrylic) Additional Tooth/UPPER", ref: "Partial Denture (Pd)"},
							{value: "Partial Denture (Acrylic) Every Additional Tooth/Lower", ref: "Partial Denture (Pd)"},
							{value: "Partial Denture (Flexible) -aadn Tooth/lower", ref: "Partial Denture (Pd)"},
							{value: "Partial Denture (PD - Acrylic) with one tooth/UPPER", ref: "Partial Denture (Pd)"},
							{value: "Partial Denture (PD -Acrylic) with one tooth/Lower", ref: "Partial Denture (Pd)"},
							{value: "Partial Denture (PD -Flexible) with one tooth/UPPER", ref: "Partial Denture (Pd)"},
							
							{value: "Composit  Veneers Per Tooth", ref: "Smile Designing"},
							{value: "Procalain Veneers per Tooth (Smile Designing)", ref: "Smile Designing"},
							{value: "Procalain Veneers per Tooth Premium (Smile Designing)", ref: "Smile Designing"},
							{value: "Wax up Full Mouth", ref: "Smile Designing"},
							{value: "Wax up Singlr Tooth", ref: "Smile Designing"},
							
							
							{value: "Anterior aesthetic restorations per tooth", ref: "Composite Filling"},
							{value: "Anterior aesthetic restorations", ref: "Composite Filling"},
							{value: "Anterior composite Mock up / teeth", ref: "Composite Filling"},
							{value: "Composite filling (Class I)", ref: "Composite Filling"},
							{value: "Composite filling (Class II)", ref: "Composite Filling"},
							{value: "Composite filling (Class III)", ref: "Composite Filling"},
							{value: "Composite filling (Class IV)", ref: "Composite Filling"},
							{value: "Composite filling (Class V)", ref: "Composite Filling"},
							
							{value: "Glass ionomer filling (Class I)", ref: "Glass Ionomer Filling"},
							{value: "Glass ionomer filling (Class I) Laser", ref: "Glass Ionomer Filling"},
							{value: "Glass ionomer filling (Class II)", ref: "Glass Ionomer Filling"},
							{value: "Glass ionomer filling (Class II) Laser", ref: "Glass Ionomer Filling"},
							{value: "Glass ionomer filling (Class V)", ref: "Glass Ionomer Filling"},
							
							{value: "Inlay - CAD/CAM(CEREC)", ref: "Inlays"},
							{value: "Inlay - Ceramic", ref: "Inlays"},
							{value: "Inlay - Composit", ref: "Inlays"},
							{value: "Inlay - Gold", ref: "Inlays"},
							{value: "Inlay - Metal (Ni-cr)", ref: "Inlays"},
							
							{value: "Onlay - CAD/CAM(CEREC)", ref: "Onlays"},
							{value: "Onlay - Ceramic", ref: "Onlays"},
							{value: "Onlay - Composit", ref: "Onlays"},
							{value: "Onlay - Gold", ref: "Onlays"},
							{value: "Onlay - Metal (Nicr)", ref: "Onlays"},
							
							{value: "Intermediate Restorative Material (IRM) filling", ref: "Temporary Filling"}];*/
		
		$scope.selectedSurface = $scope.selectedStatus = $scope.selectedSpecialty = $scope.selectedCategory = $scope.selectedTreatment = "--select one option--";
		
		$scope.surfaceClick = function(value){
			
			$scope.consultation.surface = $scope.selectedSurface = value;
		}
		$scope.statusClick = function(value){
			$scope.consultation.status = $scope.selectedStatus = value;
		}
		$scope.specialtyClick = function(value){
			$scope.consultation.specialty = $scope.selectedSpecialty = value;
			$scope.categorySave($scope.selectedSpecialty);
		}
		$scope.categoryClick = function(value){
			$scope.consultation.category = $scope.selectedCategory = value;
			$scope.treatmentSave($scope.selectedCategory);
		}
		$scope.treatmentClick = function(value){
			$scope.consultation.treatment = $scope.selectedTreatment = value;
		}
		
		
		$scope.consultation = {amount : '', category : '', performedBy : '', performedDate : '', plannedDate : '', remark : '', specialty : '', status : '', surface : '', teethNo : '', treatment : ''};
		$scope.consultationSet = [];
		$scope.saveConsultation = function (data){
			$scope.consultationSet.push(data);
			$scope.consultation = {amount : '', category : '', performedBy : '', performedDate : '', plannedDate : '', remark : '', specialty : '', status : '', surface : '', teethNo : '', treatment : ''};
		}
		
		$scope.saveConsultationData = function(data) {
			HttpService.update("save-consultation-info", data,
				function (response) {
					console.log(response.data);
				},
				function (error) {
					console.log(error);
				}
			);
		}
		
		$scope.currentTab = '';
		$scope.examinationLoad = function() {
			$scope.currentTab = 'examination';
			$scope.consultation = {amount : '', category : '', performedBy : '', performedDate : '', plannedDate : '', remark : '', specialty : '', status : '', surface : '', teethNo : '', treatment : ''};
			$scope.consultationSet = [];
		}
		
		$scope.treatmentLoad = function() {
			$scope.currentTab = 'treatment';
			$scope.consultation = {amount : '', category : '', performedBy : '', performedDate : '', plannedDate : '', remark : '', specialty : '', status : '', surface : '', teethNo : '', treatment : ''};
			$scope.consultationSet = [];
		}
		
	}]);
});