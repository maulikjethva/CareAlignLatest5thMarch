/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Home Module Controller.
 */
define(['angular', 'underscore'], function (angular, _) {
    'use strict';

    return angular.module('app.module.home.controller', [])
        .controller('HomeController', ['$timeout', '$scope', 'RealTimeChannel', 'RealTimeBedsChannel', 'HttpService', '$rootScope',
            function ($timeout, $scope, RealTimeChannel, RealTimeBedsChannel, HttpService, $rootScope) {

                $scope.centers = [];
                $scope.isConnectLoadingActive = true;
                $rootScope.isHomeActive = true;
                $rootScope.isSupervisorActive = false;
                $scope.isUserActive = false;
                $scope.isHelpActive = false;
                $scope.isProfileActive = false;
                
                $scope.isConfigActive = false;
                $scope.isCenterConfigActive = false;

//                var homeUpdate = function () {
//                };
//                homeUpdate();
                
                
                /**
                 *
                 * @param $event
                 */
                $scope.makeHomeActive = function (centerInfo) {

                    if (centerInfo.presentBeds && centerInfo.presentBeds.length === 0) {
                        $scope.isConnectLoadingActive = true;
                    }
                    else {
                        $scope.isConnectLoadingActive = false;
                    }
                };

                /**
                 * Listens for ACTIVATE_HOME command and select the Home Page.
                 */
                $scope.$on('ACTIVATE_HOME', function (event, centerInfo) {
                    $scope.makeHomeActive(centerInfo);
                });
            }]);
});