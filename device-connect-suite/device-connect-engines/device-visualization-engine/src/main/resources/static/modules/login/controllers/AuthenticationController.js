/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */


/**
 * Login Module Controller.
 */
define(['angular', 'toastr'], function (angular, toastr) {
	'use strict';
	return angular.module('ui.app.modules.login.controller', [])
		.controller('AuthenticationController', ['$scope', 'HttpService', '$location', 'RealTimeChannel', '$rootScope', 'ActiveUserInfoService','ToastrService', function ($scope, HttpService, $location, RealTimeChannel, $rootScope, ActiveUserInfoService,ToastrService) {

			$scope.userCredentials = {};
			
			$scope.sessionStart = function() {
				  HttpService.create('initDB', '',function (response) {
					  if (response.data) {
							console.log('Session Started');
						} else {
							console.log('Session Error');
						}
					},
					function (error) {
	
					});
				}
			$scope.clickHere = function(value) {
				  $scope.dispHTLM = 'modules/login/views/'+value+'.html';
				  console.log($scope.dispHTLM);
			  }
			  
			  $scope.treatmentobjCancel = function() {
				  $scope.dispHTLM = '';
			  }
			  
			/**
			 * Login using user credential.
			 */
			$scope.login = function (credentials) {
				if (credentials.username === undefined || credentials.username === null || credentials.password === undefined || credentials.password === null) {
					$scope.invalid = "Please fill the credentials";
				}
				else {
//					var headers = credentials ? {
//						authorization: "Basic " + btoa(credentials.username + ":" + credentials.password)
//					} : {};
//					HttpService.getByParms('login', {
//						headers: headers
//					}, function (response) {
//						$rootScope.userView = response.data.administrator === true; // if user is admin then to show userManagement tab 
//						$rootScope.authenticated = true;
//						$rootScope.sessionId = response.data.sessionId;
//						RealTimeChannel.init();
//						
//						ActiveUserInfoService.loadCurrentUser(response.data.name);
//						ActiveUserInfoService.setUserRoles(response.data.roles);
//						
//						var rolesGuest = 'false';
//
//						for (var i = 0; i < response.data.roles.length; i++) {
//							if (response.data.roles[i] === 'GUEST') {
//								rolesGuest = 'true';
//							}
//						}
//
//						if (rolesGuest === 'true') {
//							console.log(rolesGuest);
//							$location.path("/guest");
//						}
//						else {
//							$location.path("/home");
//						}
//
//					}
					HttpService.getByParms('authenticate-credential', { params: { userName: credentials.username, passWord: credentials.password } },
					function (response) {
						  
						  console.log(response.data);
						  if (response.data) {
								ToastrService.success('Login is successfully', 'Success');
								
							} else {
								ToastrService.error('Invalid', 'Error');
							}
						  $location.path('/home');
					  }
					, function (error) {
						$scope.invalid = "Invalid username or Password";
						$scope.userCredentials = {};
					});
				}


			};
			$scope.today = new Date();
		}]);
});