/**
 * Created by Apricot on 04-06-2016.
 */

define(['angular',
        'modules/emr/directives/emr-directive',
        'modules/emr/controllers/emr-controller'
    ],

    function(angular) {
        'use strict';
        return angular.module('ui.app.module.emr',[
            'ui.app.modules.emr.directive',
            'ui.app.modules.emr.controller'
        ]);
    });