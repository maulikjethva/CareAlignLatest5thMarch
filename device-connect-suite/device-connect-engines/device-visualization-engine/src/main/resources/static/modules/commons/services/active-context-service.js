/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Stores all Centers which are sharable to others.
 */
define(['angular'], function (angular) {
    return angular.module('ui.app.modules.commons.service.active.context', [])
        .service('ActiveContextService', [function () {

            var activeCenter;
            var activeBed;
            var activeDevice;

            /**
             * Returns active center.
             *
             * @returns {*}
             */
            this.getActiveCenter = function () {
                return activeCenter;
            };

            /**
             * Returns active bed.
             *
             * @returns {*}
             */
            this.getActiveBed = function () {
                return activeBed;
            };

            /**
             * Returns active device.
             *
             * @returns {*}
             */
            this.getActiveDevice = function () {
                return activeDevice;
            };

            /**
             * Sets the active context.
             *
             * @param context
             */
            this.setActiveContext = function (context) {
                activeCenter = context.currentCenter;
                activeBed = context.currentBed;
                activeDevice = context.currentDevice;
            };

            /**
             * Set the center
             */
            this.setCenter = function (center) {
                activeCenter = center;
            };
        }]);
});