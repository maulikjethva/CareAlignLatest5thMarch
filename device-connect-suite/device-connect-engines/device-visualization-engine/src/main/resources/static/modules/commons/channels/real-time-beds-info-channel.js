/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * The RealTimeChannel.
 * An Angular Service provider wrapper act as the Real Time Service facilitator.
 * The provider exposes different methods to communicate between Client & Server in real
 * time manner using 'websocket' or 'long-polling' as transport.
 */
define(['angular', 'atmosphere'], function (angular, atmosphere) {

    'use strict';
    return angular.module('ui.app.modules.commons.channel.realtime.beds', [])
        .service('RealTimeBedsChannel', [function () {

            var socket;
            var request;

            var configure = function (centerId) {

                closeConnection();

                var domain = window.location;
                var url = domain.protocol + '//' + domain.hostname + ':' + domain.port + '/';//domain.port
                var endpoint = 'device-connect/' + centerId; // + $rootScope.sessionId;

                var handshakeURL = url + endpoint;

                //Real time communication end-point.
                request = {
                    url: handshakeURL,
                    contentType: 'application/json',
                    logLevel: 'info', //TODO: Need to update in production.
                    transport: 'websocket',
                    fallbackTransport: 'long-polling', //To support old browsers.
                    trackMessageLength: true,
                    timeout: 10800000
                };

                var responseBackup = {};

                //Invoked upon a successful handshake.
                request.onOpen = function (response) {
                };

                //Invoked up on a message from Server.
                request.onMessage = function (response) {
                    var body = response.responseBody;
                    var jsonBody = atmosphere.util.parseJSON(body);
                    if (jsonBody) {
                        responseBackup = jsonBody;
                    } else {
                        //TODO: Need to remove but thorough testing is needed.
                        console.log('NULL Response--->' + jsonBody);
                    }
                    jsonBody = jsonBody ? jsonBody : responseBackup;

                    onServerMessage(jsonBody);
                };

                request.onError = function (response) {
                };

                request.onDisconnect = function (response) {
                };

                request.onReconnect = function (response) {
                };

                //Atmosphere invocation using the above configured request.
                socket = atmosphere.subscribe(request);
            }

            var onServerMessage = function (callback) {
                onServerMessage = callback;
            };

          
            var closeConnection = function() {
                if (socket) {
                    // atmosphere.unsubscribe(request);
                    socket.close();
                    socket = undefined;
                    request = undefined;
                }
            }

            return {
                init: configure,
                onChannelMessage: onServerMessage,
                closeConnection: closeConnection
            };

        }]);
});

