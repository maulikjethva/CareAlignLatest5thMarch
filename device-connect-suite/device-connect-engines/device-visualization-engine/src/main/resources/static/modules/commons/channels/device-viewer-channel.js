/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Stores all Centers which are sharable to others.
 */
define(['angular'], function (angular) {
    return angular.module('ui.app.modules.commons.channel.device.viewer', [])
        .service('DeviceViewerDataChannel', ['EventBusFactory', function (EventBusFactory) {

            // local constant for the channel id
            var DEVICE_VIEWER_DATA_CHANNEL = "deviceViewerMessage";

            var sendDeviceData = function(topic, data) {
                EventBusFactory.publish(DEVICE_VIEWER_DATA_CHANNEL, topic, data);
            };

            var onDeviceData = function(topic, callback) {
                EventBusFactory.subscribe(DEVICE_VIEWER_DATA_CHANNEL, topic, callback);
            };

            var stopDeviceData = function() {
                EventBusFactory.unsubscribe(DEVICE_VIEWER_DATA_CHANNEL);
            };

            return {
                sendDeviceData: sendDeviceData,
                onDeviceData: onDeviceData,
                stopDeviceData: stopDeviceData
            };
        }]);
});