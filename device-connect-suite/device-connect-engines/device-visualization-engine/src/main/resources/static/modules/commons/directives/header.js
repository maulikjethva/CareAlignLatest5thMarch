/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Application Header Widget.
 */
define(['angular'], function(angular){
    'use strict';

    return angular.module('ui.modules.commons.directives.header', [])
        .directive('appHeader', [function(){
            return{
                restrict: 'E',
                replace: true,
                templateUrl: 'modules/commons/views/header.html',
                controller: 'HeaderController',
                scope: true,
                link: function(scope, element, attributes){
                    //TODO: We may need later.
                }
            };
        }]);
});