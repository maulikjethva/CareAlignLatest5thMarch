/**
 * Copyright (c) 2016 RTWO Healthcare Solutions. All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * RTWO Healthcare Solutions. The software may be used and/or copied only
 * with the written permission of RTWO Healthcare Solutions or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * Stores all Centers which are sharable to others.
 */
define(['angular'], function (angular) {
    return angular.module('ui.app.modules.commons.channel.notification', [])
        .service('NotificationChannel', ['EventBusFactory', function (EventBusFactory) {

            // local constant for the channel id
            var NOTIFICATION_CHANNEL = "notificationMessage";

            var sendNotificationData = function (topic, data) {
                EventBusFactory.publish(NOTIFICATION_CHANNEL, topic, data);
            };

            var onNotificationData = function (topic, callback) {
            	EventBusFactory.subscribe(NOTIFICATION_CHANNEL, topic, callback);
            };

            var stopNotificationData = function () {
                EventBusFactory.unsubscribe(NOTIFICATION_CHANNEL);
            };

            return {
                sendNotificationData: sendNotificationData,
                onNotificationData: onNotificationData,
                stopNotificationData: stopNotificationData
            };
        }]);
});