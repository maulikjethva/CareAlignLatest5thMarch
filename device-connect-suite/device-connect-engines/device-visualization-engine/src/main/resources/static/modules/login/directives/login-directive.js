define(['angular'], function(angular) {
    'use strict';

    return angular.module('ui.app.modules.login.directive', [])
        .directive('login', [function () {
            return {
                restrict: 'E',
                replace:true,
                templateUrl:'modules/login/views/login.html',
            	controller: 'AuthenticationController',
                scope: true,

                link: function(scope, element, attributes){
                    scope.template = 'modules/login/views/sign-up.html';
                    scope.selectedLi = 'appointment-registration';
                                                            
                    scope.getEmrHtml = function(val){
                        scope.selectedLi = val;
                        scope.template = 'modules/emr/views/'+val+'.html';
                        
                        if(val = 'consultation') {
                        	scope.examinationLoad();
                        }
                    }
                }
            };
        }]);
});