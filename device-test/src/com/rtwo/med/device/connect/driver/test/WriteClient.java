package com.rtwo.med.device.connect.driver.test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class WriteClient{
	
	//static byte[] byteArr = new byte[]{0x3C, 0x0A, 0x3C, 0x0A, 0x3C, 0x0A, 0x3C, 0x0A, 0x3C, 0x0A, 0x3C, 0x0A, 0x3C, 0x0A, 0x3C, 0x0A, 0x3C, 0x0A, 0x3C};
	static byte[] byteArr = new byte[]{(byte) 0xFA, (byte) 0xCA, 0x0A, 0x01, 0x54, 0x01, 0x16, 0x00, (byte) 0xA8, 0x00, (byte) 0xA8, 0x00, 0x00, 0x3C, 0x3C, 0x3C, 0x3C, 0x10, (byte) 0x80};

	public static void main(String[] args) {
		
		Writer writer = new Writer("localhost", "8084", byteArr);
		Thread writerWorker = new Thread(writer);
		writerWorker.start();
	}
	
    
    private static int count = 1;
    
    public WriteClient()
    {
    }
    
    private static class Writer implements Runnable{
    	
    	private Socket socket;
    	
        private String ip;
        
        private String port;
        
        private byte[] data;
        
    	Writer(String ip, String port, byte[] data){
    		
    		this.ip = ip;
        	this.port = port;
        	this.data = data;
    	}
    	
    	public void run() {

    		while(true){
    			
    			try {
    				
    				this.socket = new Socket(ip, Integer.parseInt(port));
    				OutputStream out = this.socket.getOutputStream();
    				//out.write(new String("Hello I am Client-"+count).getBytes());
    				out.write(data);
    				out.flush();
    				out.close();
    				count++;
    				Thread.sleep(5000L);
    			} 
    			catch (NumberFormatException e1) {
    				
    				System.out.println("NumberFormatException: " + this.port);
    				System.exit(1);
    				
    			} 
    			catch (UnknownHostException uhe){
    				
    	            System.out.println("Don't know about host: " + this.ip);
    	            System.exit(1);
    	        }
    	        catch (IOException ioe){
    	        	
    	            System.out.println("Couldn't get I/O for the connection to: " + this.ip + ":" + this.port);
    	            System.exit(1);
    	        } 
    			catch (InterruptedException e) {

    				System.out.println("InterruptedException: " + e);
    				System.exit(1);
    			}
    		}
    		
    	}
    }
}
